<?php session_start(); ?>

<html lang="en">

<head>
	<title>Shopping Cart</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="login/css/style.css">
</head>

<body background="image/aa.png">

<div class="container-fluid">
    <div class="col-md-12 mt-4">
      <div class="row justify-content-md-center">
        <div class="col-md-auto"><?php echo $check_submit; ?></div>
      </div>
    </div>
    <div class="row justify-content-md-center">
      <div class="col-md-4">
        <div class="card border-dark mt-2">
          <center>
          <h5 class="card-header"> เข้าสู่ระบบ <i class="fa fa-sign-in" aria-hidden="true"></i></h5></center>
          <div class="card-body">
            <div class="row justify-content-md-center mb-2">
              <div class="col col-lg-6">
                <img src="images/braner.png" alt="" width="200px">
              </div>
            </div>
            <form method="post" action="cheack_login.php">
              <div class="form-group">
                <label>ชื่อผู้ใช้</label>
                <input type="text" class="form-control" name="user_username" value="<?php echo $user_username; ?>" placeholder="Enter Username" required="" />
              </div>
              <div class="form-group">
                <label>รหัสผ่าน</label>
                <input type="password" class="form-control" name="user_password" placeholder="Enter Password" required="" />
              </div>
              <center>
                <button type="submit" class="btn btn-success" name="submit">เข้าสู่ระบบ</button>
                <a class="btn btn-warning" href="member_form_add.php" role="button">สมัครสมาชิก</a>
              </center>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div><br>



	<!-- <form class="login100-form validate-form p-b-33 p-t-5" action="cheack_login.php" method="POST">
		<section class="ftco-section">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-6 text-center mb-5">
						<h2 class="heading-section">Login Shopping Cart</h2>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-md-6 col-lg-5">
						<div class="login-wrap p-4 p-md-5">
							<div class="icon d-flex align-items-center justify-content-center">
								<span class="fa fa-user-o"></span>
							</div>
							<h3 class="text-center mb-4">Have an account?</h3>
							<form action="#" class="login-form">
								<div class="form-group">
									<input type="text" name="user_username" class="form-control rounded-left" placeholder="Username" required>
								</div>
								<div class="form-group d-flex">
									<input type="password" name="user_password" class="form-control rounded-left" placeholder="Password" required>
								</div>
								<div class="form-group d-md-flex">
									<div class="w-50">
										<label class="checkbox-wrap checkbox-primary">Remember Me
											<input type="checkbox" checked>
											<span class="checkmark"></span>
										</label>
									</div>
									<div class="w-50 text-md-right">
										<a href="#">Forgot Password</a>
									</div>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary rounded submit p-3 px-5">เข้าสู่ระบบ</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	</form> -->
	<script src="login/js/jquery.min.js"></script>
	<script src="login/js/popper.js"></script>
	<script src="login/js/bootstrap.min.js"></script>
	<script src="login/js/main.js"></script>
</body>

</html>