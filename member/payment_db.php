<?php
// echo '<pre>';
// print_r($_POST);
// echo '<pre>';

session_start();
// echo '<meta charset="utf-8">';
include('../condb.php');
// echo "<pre>";
// print_r($_POST);
// echo "</pre>";
// exit();
if ($_SESSION['user_username'] != 'member') {
	Header("Location: index.php");
}

$book_slip_date = Date("Y-m-d G:i:s");
$book_id = mysqli_real_escape_string($con, $_POST["book_id"]);


$date1 = date("Ymd_His");
$numrand = (mt_rand()); //mt_rand มีการสุ่มตัวเลขเพื่อเปลี่ยนไฟล์ที่ไม่ให้ภาพมันซ้ำกัน
$book_slip = (isset($_POST['book_slip']) ? $_POST['book_slip'] : '');
$upload = $_FILES['book_slip']['name'];
if ($upload != '') {

	$path = "../imgslip/"; //Folder ที่ทำการเก็บ file img
	$type = strrchr($_FILES['book_slip']['name'], "."); //ตัวแปรนี้คือ การนำไฟล์มาตัดชื่ออกแล้วเหลือเพียงนามสกุลของไฟล์นั้น
	$newname = 'slip' . $numrand . $date1 . $type; //เมื่อตัดชื่อออกเรา ทำการเปลียนชื่อเป็น slip และสุ่มตัวอักษรต่อด้วยวันที่ปัจจุบันต่อด้วยนามสกุล
	$path_copy = $path . $newname; //การเก็บพาทเข้าไปในคอลัมล์
	$path_link = "../imgslip/" . $newname;
	move_uploaded_file($_FILES['book_slip']['tmp_name'], $path_copy);  //การ coppyfile แล้วไปใส่ใน folder ที่เราได้สร้างไว้
} else {
	$newname = ''; //ถ้าไม่มีการอัปโหลดมาเราจะให้เกิดค่าว่างเลย
}

$sql = "UPDATE tb_booking SET book_status=3,book_slip_date='$book_slip_date',book_slip='$newname'WHERE book_id=$book_id";
$result = mysqli_query($con, $sql) or die("Error in query: $sql " . mysqli_error($con));

mysqli_close($con);

if ($result) {
	echo '<script>';
	echo "alert('แจ้งชำระเงินสำเร็จ');";
	echo "window.location='payment_detail.php?book_id=$book_id';";
	echo '</script>';
} else {
	echo '<script>';
	echo "alert('Error!!!');";
	echo "window.location='index.php';";
	echo '</script>';
}
