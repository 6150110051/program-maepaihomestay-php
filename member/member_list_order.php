<?php

$queryorder =  "SELECT * FROM tb_booking WHERE user_id = $user_id";
$rsorder = mysqli_query($con, $queryorder) or die("Error in query : $queryorder" . mysqli_error($con));

// echo $row['book_id'];
// echo $queryorder;
// exit;
?>



<h3>รายการจอง</h3>
<table class="table table-border table-hover table-bordered table-striped " id="Table" width="790px">
    <thead>
        <tr>

            <th style="text-align:left;" width="20%">สถานะ</th>
            <th style="text-align:left;" width="40%">วันที่จอง</th>
            <th style="text-align:left;" width="15%">รวม</th>
            <th style="text-align:left;" width="5%">หลักฐานการโอน</th>
            <th style="text-align:left;" align="center" width="100%">รายละเอียด</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($rsorder as $row) { ?>
            <tr>

                <td>
                    <?php
                    $st = $row['book_status'];
                    if ($st == 1) {
                        echo '<font color = "warning">';
                        echo 'รออนุมัติ';
                        echo '</font>';
                    } elseif ($st == 2) {
                        echo '<font color = "orage">';
                        echo 'อนุมัติ';
                        echo '</font>';
                    } elseif ($st == 3) {
                        echo '<font color = "green">';
                        echo 'จองสำเร็จ';
                        echo '</font>';
                    } elseif ($st == 4) {
                        echo '<font color = "red">';
                        echo 'ยกเลิกการจอง';
                        echo '</font>';
                    } //1 = รออนุมัติ 2 = อนุมัติ(รอชำระเงิน) 3 = จองสำเร็จ 4 = ยกเลิก	

                    ?>
                </td>
                <td><?php echo $row['book_date']; ?></td>
                <td align="right"><?php echo '฿ ' . number_format($row['book_total'], 2);  ?></td>
                <td><img src="../imgslip/<?php echo $row['book_slip']; ?>" width="100px" height="auto"></td>
                <td align="center">
                    <?php
                    $book_id = $row['book_id'];
                    $st = $row['book_status'];
                    if ($st == 1) {
                        echo "<a href = 'payment.php?book_id=$book_id&do=pament' class = 'btn btn-secondary btn-sm'> 
                            รออนุมัติ </a>";
                    } elseif ($st == 2) {
                        echo "<a href = 'payment.php?book_id=$book_id&do=pament' class = 'btn btn-primary btn-sm'> 
                         ชำระค่ามัดจำ </a>";
                    } elseif ($st == 3) {
                        echo "<a href = 'payment_detail.php?book_id=$book_id&do=payment_detail' class = 'btn btn-success btn-sm'>
                            เปิดดู </a>";
                    } elseif ($st == 4) {
                        echo "<a href = '#' class = 'btn btn-danger btn-sm'> 
                            ยกเลิก </a>";
                    } //1 = รออนุมัติ 2 = อนุมัติ(รอชำระเงิน) 3 = จองสำเร็จ 4 = ยกเลิก	
                    ?>
                </td>
            </tr>
        <?php } ?>
    </tbody>

</table>