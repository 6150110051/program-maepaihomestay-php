<?php
$book_id = $_GET['book_id'];
include("h_nav_member.php");


$querycartdetail =  "SELECT * FROM bookingroomdetail brd 
INNER JOIN tb_booking bk ON brd.book_id = bk.book_id 
INNER JOIN tb_room r ON r.room_id = brd.room_id 
WHERE brd.book_id = $book_id";
$rscartdetail = mysqli_query($con, $querycartdetail) or die("Error in query : $querycartdetail" . mysqli_error($con));
$rowdetail = mysqli_fetch_array($rscartdetail);
$records1 = $rscartdetail->num_rows;

// sql activity
$querycartdetail2 =  "SELECT * FROM bookingactivitydetail bad 
INNER JOIN tb_booking bk ON bad.book_id = bk.book_id 
INNER JOIN tb_activity a ON a.activity_id = bad.activity_id 
WHERE bad.book_id = $book_id";
$rscartdetail2 = mysqli_query($con, $querycartdetail2) or die("Error in query : $querycartdetail2" . mysqli_error($con));
$rowdetail2 = mysqli_fetch_array($rscartdetail2);
$records2 = $rscartdetail2->num_rows;

$records = $records1 + $records2;

// echo "<pre>";
// print_r($records);
// echo "<pre>";
// echo '<pre>';
// print_r($rowdetail);
// echo '<hr>';
// print_r($rowdetail2);
// // echo $records2;
// echo '<pre>';
?>
<title>Document</title>

<link rel="stylesheet" href="../header/css/style.css">

<link rel="stylesheet" href="//cdn.datatables.net/1.11.2/css/jquery.dataTables.min.css">


<!-- card -->
<link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Roboto:wght@300&display=swap" rel="stylesheet">

<style>
  .main-content {
    padding-top: 100px;
    padding-bottom: 100px;
  }

  a {
    text-decoration: none;
  }

  .food-card {
    background: #fff;
    border-radius: 5px;
    overflow: hidden;
    margin-bottom: 30px;
    -webkit-box-shadow: 0 2px 10px rgba(0, 0, 0, 0.06);
    box-shadow: 0 2px 10px rgba(0, 0, 0, 0.06);
    -webkit-transition: 0.1s;
    transition: 0.1s;
  }

  .food-card:hover {
    -webkit-box-shadow: 0 5px 20px rgba(0, 0, 0, 0.1);
    box-shadow: 0 5px 20px rgba(0, 0, 0, 0.1);
  }

  .food-card .food-card_img {
    display: block;
    position: relative;
  }

  .food-card .food-card_img img {
    width: 100%;
    height: 200px;
    -o-object-fit: cover;
    object-fit: cover;
  }

  .food-card .food-card_img i {
    position: absolute;
    top: 20px;
    right: 30px;
    color: #fff;
    font-size: 25px;
    -webkit-transition: all 0.1s;
    transition: all 0.1s;
  }

  .food-card .food-card_img i:hover {
    top: 18px;
    right: 28px;
    font-size: 29px;
  }

  .food-card .food-card_content {
    padding: 15px;
  }

  .food-card .food-card_content .food-card_title-section {
    height: 100px;
    overflow: hidden;
  }

  .food-card .food-card_content .food-card_title-section .food-card_title {
    font-size: 24px;
    color: #333;
    font-weight: 500;
    display: block;
    line-height: 1.3;
    margin-bottom: 8px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .food-card .food-card_content .food-card_title-section .food-card_author {
    font-size: 15px;
    display: block;
  }

  .food-card .food-card_content .food-card_bottom-section .space-between {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
  }

  .food-card .food-card_content .food-card_bottom-section .food-card_subscribers {
    margin-left: 17px;
  }

  .food-card .food-card_content .food-card_bottom-section .food-card_subscribers img,
  .food-card .food-card_content .food-card_bottom-section .food-card_subscribers .food-card_subscribers-count {
    height: 45px;
    width: 45px;
    border-radius: 45px;
    border: 3px solid #fff;
    margin-left: -17px;
    float: left;
    background: #f5f5f5;
  }

  .food-card .food-card_content .food-card_bottom-section .food-card_subscribers .food-card_subscribers-count {
    position: relative;
  }

  .food-card .food-card_content .food-card_bottom-section .food-card_subscribers .food-card_subscribers-count span {
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    font-size: 13px;
  }

  .food-card .food-card_content .food-card_bottom-section .food-card_price {
    font-size: 25px;
    font-weight: 500;
    color: #F47A00;
  }



  .food-card .food-card_content .food-card_bottom-section input {
    background: #f5f5f5;
    border-color: #f5f5f5;
    -webkit-box-shadow: none;
    box-shadow: none;
    text-align: center;
  }

  .food-card .food-card_content .food-card_bottom-section {
    border-color: #f5f5f5;
    border-width: 3px;
    -webkit-box-shadow: none;

    box-shadow: none;
  }

  @media (min-width: 992px) {
    .food-card--vertical {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      position: relative;
      height: 235px;
    }

    .food-card--vertical .food-card_img img {
      width: 300px;
      height: 100%;
      -o-object-fit: cover;
      object-fit: cover;
    }
  }
</style>

</head>

<body>

  <div class="container" style="padding-top: 100px;">
    <div class="row">
      <div class="col-md-3">
        <div class="food-card">
          <?php include("menu_l.php"); ?>
        </div>
      </div>

      <div class="col-md-9">
        <div class="food-card">
          <div class="food-card_content">
            <h1>
              <i class="glyphicon glyphicon-user hidden-xs"></i> <span class="hidden-xs">รายละเอียดและช่องทางการชำระเงิน</span>
            </h1>
          </div>
        </div>
        <div class="food-card">
          <div class="food-card_content">
            <?php
            if ($rowdetail != ' ') {
              foreach ($rscartdetail as $row) {

            ?>

                <div class="col-md-12 food-card">
                  <strong>รหัสใบจอง : <?php echo $row['book_id']; ?></strong> <br>
                  ชื่อผู้จอง : <?php echo $row['book_name']; ?> <br>
                  เบอร์โทร : <?php echo $row['book_phone']; ?> <br>
                  Email : <?php echo $row['book_email']; ?> <br>
                  วันที่จอง : <?php echo $row['book_date']; ?> <br>
                  สถานะ : <?php
                          $st = $row['book_status'];
                          if ($st == 1) {
                            echo '<font color = "warning">';
                            echo 'รออนุมัติ';
                            echo '</font>';
                          } elseif ($st == 2) {
                            echo '<font color = "orage">';
                            echo 'อนุมัติ';
                            echo '</font>';
                          } elseif ($st == 3) {
                            echo '<font color = "green">';
                            echo 'จองสำเร็จ';
                            echo '</font>';
                          } elseif ($st == 4) {
                            echo '<font color = "red">';
                            echo 'ยกเลิกการชำระ';
                            echo '</font>';
                          } //1 = รออนุมัติ 2 = อนุมัติ(รอชำระเงิน) 3 = จองสำเร็จ 4 = ยกเลิก	
                          ?>
                  <div class="row food-card_content">

                    <div class='col-md-4 '><img src="../image/img_room/<?php echo $row["room_picture1"]; ?>" width="100%" class="cart-item-image" /></div>
                    <div class='col-md-4'>
                      <div class="row"><?php echo " ชื่อห้อง " . " : " . $row["room_name"]; ?></div>
                      <div class="row"><?php echo " รหัสห้อง " . " : " . $row["room_id"]; ?> </div>
                      <div class="row"><?php echo " วันที่เข้า " . " : " .   $row["r_check_in"]; ?> </div>
                      <div class="row"><?php echo " วันที่ออก " . " : " .   $row["r_check__out"]; ?> </div>
                    </div>

                    <div class='col-md-1'>
                      <div class="row">ผู้ใหญ่ </div>
                      <div class="row">เด็ก </div>
                      <div class="row">รวม</div>
                    </div>
                    <div class='col-md-1 '>
                      <div class="row"><?php echo $row["r_num_adult"]; ?></div>
                      <div class="row"><?php echo $row["r_num_child"]; ?></div>
                    </div>
                    <div class='col-md-2 '>
                      <?php
                      $desposit = $row["r_price"] * (20 / 100);
                      $overdue = $row["r_price"] - $desposit;
                      ?>
                      <div class="row"><?php echo  "฿ " . number_format($row["room_price_adult"], 2); ?></div>
                      <div class="row"><?php echo  "฿ " . number_format($row["room_price_child"], 2); ?></div>
                      <div class="row"><?php echo  "฿ " . number_format($row["r_price"], 2); ?></div>
                    </div>

                  </div>
                </div>
            <?php
              } //closs foreach   
            }
            ?>
            <?php
            if ($rowdetail2 != ' ') {
              foreach ($rscartdetail2 as $row) {
            ?>
                <div class="col-md-12 food-card">
                  <strong>รหัสใบจอง : <?php echo $row['book_id']; ?></strong> <br>
                  ชื่อผู้จอง : <?php echo $row['book_name']; ?> <br>
                  เบอร์โทร : <?php echo $row['book_phone']; ?> <br>
                  Email : <?php echo $row['book_email']; ?> <br>
                  วันที่จอง : <?php echo $row['book_date']; ?> <br>
                  สถานะ : <?php
                          $st = $row['book_status'];
                          if ($st == 1) {
                            echo '<font color = "warning">';
                            echo 'รออนุมัติ';
                            echo '</font>';
                          } elseif ($st == 2) {
                            echo '<font color = "orage">';
                            echo 'อนุมัติ';
                            echo '</font>';
                          } elseif ($st == 3) {
                            echo '<font color = "green">';
                            echo 'จองสำเร็จ';
                            echo '</font>';
                          } elseif ($st == 4) {
                            echo '<font color = "red">';
                            echo 'ยกเลิกการชำระ';
                            echo '</font>';
                          } //1 = รออนุมัติ 2 = อนุมัติ(รอชำระเงิน) 3 = จองสำเร็จ 4 = ยกเลิก	
                          ?>
                  <div class="row food-card_content">
                    <div class='col-md-4 '><img src="../image/img_activity/<?php echo $row["activity_picture1"]; ?>" width="100%" class="cart-item-image" /></div>
                    <div class='col-md-4'>
                      <div class="row"><?php echo " กิจกรรม " . " : " . $row["activity_name"]; ?></div>
                      <div class="row"><?php echo " รหัสกิจกรรม " . " : " . $row["activity_id"]; ?> </div>
                      <div class="row"><?php echo " วันที่เข้าร่วมกิจกรรม " . " : " .   $row["activity_date"]; ?> </div>
                      <div class="row"><?php echo " ช่วงเวลาของการเข้ากิจกรรม " . " : " . $row["activity_time"]; ?> </div>
                    </div>

                    <div class='col-md-1'>
                      <div class="row">ผู้ใหญ่ </div>
                      <div class="row">เด็ก </div>
                      <div class="row">รวม</div>
                    </div>
                    <div class='col-md-1 '>
                      <div class="row"><?php echo $row["a_num_adult"]; ?></div>
                      <div class="row"><?php echo $row["a_num_child"]; ?></div>
                    </div>
                    <div class='col-md-2 '>
                      <?php
                      $desposit = $row["a_price"] * (20 / 100);
                      $overdue = $row["a_price"] - $desposit;
                      ?>
                      <div class="row"><?php echo  "฿ " . number_format($row["activity_price_adult"], 2); ?></div>
                      <div class="row"><?php echo  "฿ " . number_format($row["activity_price_child"], 2); ?></div>
                      <div class="row"><?php echo  "฿ " . number_format($row["a_price"], 2); ?></div>
                    </div>

                  </div>
                </div>
            <?php
              } //closs foreach   
            }
            ?>

            <?php
            $querybooking =  "SELECT * FROM tb_booking WHERE book_id = $book_id";
            $rsbooking = mysqli_query($con, $querybooking) or die("Error in query : $querybooking" . mysqli_error($con));
            $row = mysqli_fetch_array($rsbooking);
            // echo '<pre>';
            // print_r($querybooking);
            // echo '<pre>';
            ?>
            <div class="food-card">
              <div class="food-card_content">
                <strong>รวมทั้งหมด <?php echo $records; ?> รายการ </strong> <br>
                คิดเป็นเงิน <?php echo  number_format($row["book_total"], 2) . " บาท "; ?>
                <hr>
                <div class="food-card_bottom-section">
                  <div class="food-card_price">
                    <p><span>จำนวนเงินที่ต้องชำระมัดจำ : </span></strong><?php echo  number_format($row["book_desposit"], 2); ?> <strong> บาท</strong></p>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
      <?php
      if ($st == 2) {
      ?>
        <div class="col-md-3">
        </div>
        <div class="col-md-9">
          <div class="food-card">
            <div class="food-card_content">
              <!-- enctype = "multipart/form-data" ตัวนี้ไว้ใช้สำหรับการอัพโหลดไฟล์ทุกประเภท -->
              <h4>ช่องทางการชำระเงิน</h4>
              <hr>
              <div class="food-card">
                <div class="food-card_content">
                  <form action="payment_db.php" method="POST" class="form-horuzontal" enctype="multipart/form-data">
                    <strong>
                      <p>ชำระผ่าน Mobile Banking</p>
                    </strong>
                    <p>- ชำระผ่าน K-Mobile Banking บัญชีธนาคารกสิกรไทย</p>
                    <p> ***เลขบัญชีธนาคาร <strong>0163 625 395 </strong>นาย แก้ววิเศษพงศ์ สงวนวงศ์</p>
                    <p>- ชำระผ่าน BualuangM บัญชีธนาคารกรุงเทพ</p>
                    <p> ***เลขบัญชีธนาคาร <strong>0163 625 395</strong> นาย แก้ววิเศษพงศ์ สงวนวงศ์</p>
                    <hr>

                    <div class="row-md-12 form-group">
                      <div class="col-md-3">
                        จำนวนเงินที่ต้องโอน <br>
                        <div class="food-card_bottom-section">
                          <div class="food-card_price">
                            <p><span></span></strong><?php echo  number_format($row["book_desposit"], 2); ?> <strong> บาท</strong></p>
                          </div>
                        </div>

                      </div>
                      <div class="form-group">
                        <div class="col-md-4">
                          อัพโหลดภาพวลิป <br>
                          <input type="file" name="book_slip" required class="form-control" accept="image/*">
                        </div>
                      </div>
                    </div>
                    <div class="row-md-12 form-group">
                      <div class="col-md-2">
                        <br>
                        <input type="hidden" name="book_id" value="<?php echo $book_id; ?>">
                        <button type="submit" class="btn btn-primary">แจ้งชำระเงิน</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php
      }
      ?>
    </div>
  </div>
  </div>
</body>

</html>
<?php include('footerjs.php'); ?>