<?php
session_start();
include("../condb.php");
include("head.php");
$user_level = $_SESSION['user_level'];
//  print_r($_SESSION);
?>
<header class="header">
    <!-- nav -->
    <div class="header-inner">
        <div class="container-fluid px-lg-5">
            <nav class="navbar navbar-expand-lg my-navbar ">
                <a class="navbar-brand" href="#"><span class="logo">
                        <img src="../image/braner.png" class="img-fluid" style="width:120px; margin:-3px 0px 0px 0px;">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"><i class="fas fa-bars" style="margin:5px 0px 0px 0px;"></i></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav m-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="../index.php">หน้าหลัก<span class="sr-only">(current)</span></a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">จองรายการ </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="../product_room.php">จองห้องพัก</a>
                                <a class="dropdown-item" href="../product_activity.php">จองกิจกรรม</a>

                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">แกลลอรี่</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">เกี่ยวกับโฮมสเตย์</a>
                        </li>
                    </ul>
                    <div class="social-links " style="margin-right: 30px;">
                        <a href="https://1th.me/yvrKg"><i class="fab fa-facebook-f"></i></a>&nbsp;
                        <a href="#"><i class="fab fa-twitter"></i></a>&nbsp;
                        <a href="https://1th.me/0fz8r"><i class="fab fa-instagram"></i></a>&nbsp;
                        <a href="https://1th.me/wLvxQ"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                    <form class="form-inline my-2 my-lg-0">
        
                        <?php
                        if ($user_level == "member") {
                        ?><a href="../cart.php" class="header-btn my-2 my-sm-0 nav-link ">ตะกร้า</a>
                            <a href="index.php" class="header-btn my-2 my-sm-0 nav-link ">สวัสดีคุณ <?php echo $_SESSION['user_name']; ?></a>
                        <?php } else { ?>
                            <a href="register.php" class="header-btn my-2 my-sm-0 nav-link ">สมัครสมาชิก</a>
                            <a href="form_login.php" class="header-btn my-2 my-sm-0 nav-link">เข้าสู่ระบบ</a>
                        <?php } ?>
                    </form>
                </div>
            </nav>

        </div>
    </div>
    <!-- nav -->



</header>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript">
    $(function() {
        var navbar = $('.header-inner');
        $(window).scroll(function() {
            if ($(window).scrollTop() <= 40) {
                navbar.removeClass('navbar-scroll');
            } else {
                navbar.addClass('navbar-scroll');
            }
        });
    });
</script>