<?php
if (@$_GET['do'] == 'finish') {
  echo '<script type="text/javascript">
          swal("", "แก้ไขสำเร็จ !!", "success");
          </script>';
  echo '<meta http-equiv="refresh" content="1;url=member_profile.php" />';
}
$sql = "SELECT * FROM tbl_member WHERE member_id=$member_id";
// $result = mysqli_query($con, $sql) or die ("Error in query: $sql " . mysqli_error($con));
// $row = mysqli_fetch_array($result);
?>
<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  } 
  <h1 > < i class = "glyphicon glyphicon-user hidden-xs" > </i> <span class="hidden-xs">แก้ไขข้อมูลส่วนตัว</span > </h1>
</script>
<form action="member_profile_edit_db.php" method="post" class="form-horizontal" enctype="multipart/form-data">

  <div class="mb-3">
    <label class="form-label">ชื่อผู้ใช้</label>
    <input type="text" class="form-control" name="user_username" value="<?php echo $result['user_username']; ?>" required />
  </div>
  <div class="mb-3">
    <label class="form-label">ชื่อ</label>
    <input type="text" class="form-control" name="user_name" value="<?php echo $result["user_name"]; ?>" required />
  </div>
  <div class="mb-3">
    <label class="form-label">นามสกุล</label>
    <input type="text" class="form-control" name="user_surname" value="<?php echo $result["user_surname"]; ?>" required />
  </div>
  <div class="mb-3">
    <label class="form-label">เพศ</label>
    <select class="form-select" name="user_sex">
      <option value="ชาย" <?php if ($result["user_sex"] == 'ชาย') {
                            echo " selected";
                          } ?>>ชาย</option>
      <option value="หญิง" <?php if ($result["user_sex"] == 'หญิง') {
                              echo " selected";
                            } ?>>หญิง</option>
    </select>
  </div>
  <div class="mb-3">
    <label class="form-label">วันเกิด</label>
    <input type="date" class="form-control" name="user_birthdate" value="<?php echo $result["user_birthdate"]; ?>" required />
  </div>
  <div class="mb-3">
    <label class="form-label">เบอร์</label>
    <input type="text" class="form-control" name="user_phone" value="<?php echo $result["user_phone"]; ?>" required />
  </div>
  <div class="mb-3">
    <label class="form-label">ไลน์</label>
    <input type="text" class="form-control" name="user_line" value="<?php echo $result["user_line"]; ?>" required />
  </div>
  <div class="mb-3">
    <label class="form-label">เฟสบุ๊ค</label>
    <input type="text" class="form-control" name="user_facebook" value="<?php echo $result["user_facebook"]; ?>" required />
  </div>
  <div class="mb-3">
    <label class="form-label">อีเมล์</label>
    <input type="email" class="form-control" name="user_email" value="<?php echo $result["user_email"]; ?>" required />
  </div>

  <div class="mb-3">
    <label class="form-label">ระดับผู้ใช้</label>
    <select class="form-select" name="user_level">
      <option value="member" <?php if ($result["user_level"] == 'member') {
                                echo " selected";
                              } ?>>สมาชิก</option>
      <option value="admin" <?php if ($result["user_level"] == 'admin') {
                              echo " selected";
                            } ?>>ผู้ดูแลระบบ</option>
    </select>
  </div>





  <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="window.location.href='member.php'">ยกเลิก</button>
                        <button type="submit" name="submit" class="btn btn-primary">บันทึก</button>
                      </div>
</form>