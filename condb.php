<?php
//funtion
date_default_timezone_set('Asia/Bangkok');

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "maepaihomestay";

// Create Connection
$con = mysqli_connect($servername, $username, $password, $dbname) or die("Error: ".$con. mysqli_error($con));

mysqli_query($con, "SET NAMES 'utf8' ");
error_reporting( error_reporting() & ~E_NOTICE );

// Check connection
if (!$con) {
    die("Connection failed" . mysqli_connect_error());
    echo 'ทำการเชื่อต่อผิดพลาด' ;
} 

class DBroomController {
	private $host = "localhost";
	private $user = "root";
	private $password = "";
	private $database = "maepaihomestay";
	private $con;
	
	// รันอัตโนมัติ
	function __construct() {
		$this->con = $this->connectDB();
	}
	
	function connectDB() {
		$con = mysqli_connect($this->host,$this->user,$this->password,$this->database);
		return $con;
	}
	
	// เมื่อฟังชันตัวนี้ถูกเรียกใช้แล้วจะทำงาน เป็นตัวQuery
	function runQuery($query) {
		$result = mysqli_query($this->con,$query);
		// เมื่อเข้าลูปทำการสร้างตัวแปร row ส่งตัวแปร result เข้ามา
		while($row=mysqli_fetch_assoc($result)) {
			// เก็บข้อมูล
			$resultset[] = $row;
		}		
		// ถ้ามีข้อมูลจะรีเทินค่า resultset กลับ
		if(!empty($resultset))
			return $resultset;
	}
	
	function numRows($query) {
		$result  = mysqli_query($this->con,$query);
		$rowcount = mysqli_num_rows($result);
		return $rowcount;	
	}
}

?>

