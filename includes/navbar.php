<?php
if ($_SESSION != NULL) {
  $sql_tb_user = "SELECT * FROM tb_user WHERE user_username = '".$_SESSION['user_username']."'";
  $query_tb_user = mysqli_query($Connection,$sql_tb_user);
  $result_tb_user = mysqli_fetch_array($query_tb_user,MYSQLI_ASSOC);
}
?>
<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-info">
  <span class="navbar-brand"><?php echo $title; ?></span>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php"><i class="fa fa-home fa-lg"></i> หน้าหลัก <span class="sr-only">(current)</span></a>
      </li>
    </ul>
    <?php
    if ($_SESSION == NULL) {
      ?>
      <button type="submit" class="btn btn-outline-primary my-2 my-sm-0" onclick="window.location.href='login.php'">เข้าสู่ระบบ</button>
      <?php
    }else{
      ?>
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo "<i class='fa fa-user-circle-o fa-lg'></i> ".$result_tb_user["user_name"]." ".$result_tb_user["user_surname"]; ?></a>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="profile.php">ข้อมูลส่วนตัว</a>
            <?php
            if ($_SESSION["user_level"] == "admin") {
              ?>
              <a class="dropdown-item" href="admin/index.php">ระบบหลังบ้าน</a>
              <?php
            }
            ?>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="logout.php"><i class="fa fa-sign-out"></i> ออกจากระบบ</a>
          </div>
        </li>
      </ul>
      <?php
    }
    ?>
  </div>
</nav>
