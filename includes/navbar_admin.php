<?php
$sql_tb_user = "SELECT * FROM tb_user WHERE user_username = '" . $_SESSION['user_username'] . "'";
$query_tb_user = mysqli_query($Connection, $sql_tb_user);
$result_tb_user = mysqli_fetch_array($query_tb_user, MYSQLI_ASSOC);
?>

<!-- Main Header -->
<header class="main-header ">

  <!-- Logo -->
  <a href="index.php" class="logo">
    <!-- logo for regular state and mobile devices -->
    <!-- <span class="logo-lg"><b>แม่ไพโฮมสเตย์</span> -->
    <img src="../images/braner.png" alt="" width="100px">
  </a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top p-0" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="index.php" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only"></span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav px-3">

        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
          <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <!-- The user image in the navbar-->
            <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt=""> -->
            <!-- hidden-xs hides the username on small devices so only the image appears. -->
            <span class="hidden-xs"><?php echo "<i class='fa fa-user-circle-o fa-lg'></i> " . $result_tb_user["user_name"] . " " . $result_tb_user["user_surname"]; ?></span>
          </a>
          <ul class="dropdown-menu ">
            <!-- The user image in the menu -->
            <li class="user-header">
              <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->

              <p><br>
                <b>Admin</b><br>
                ระบบจองห้องพักแม่ไพโฮมสเตย์ <br><br>

                <small>วันที่ <?php
                              date_default_timezone_set('asia/bangkok');
                              $date = date(" d - m - Y");
                              echo $date;
                              ?> </small>
              </p>
            </li>

            <!-- Menu Body -->
            <!-- <li class="user-body">
              <div class="row">
                <div class="col-xs-4 text-center">
                  <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Friends</a>
                </div>
              </div> -->
            <!-- /.row -->
            <!-- </li> -->

            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="profile_admin.php" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="../logout.php" class="btn btn-default btn-flat">Sign out</a>
                <!-- <a href=""><i class="fa fa-sign-out"></i>Sign out</a> -->
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->

      </ul>
    </div>
  </nav>
</header>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu p-0" data-widget="tree">
      <!-- <li class="header">HEADER</li> -->
      <!-- Optionally, you can add icons to the links -->
      <li class="active"><a href="index.php"><i class="fa fa-home"></i> <span>แม่ไพโฮมสเตย์</span></a></li>

      <li><a href="index.php"><i class="fa fa-pie-chart" aria-hidden="true"></i> <span>หน้าหลัก</span></a></li>
      <li><a href="room.php"><i class="fa fa-bed" aria-hidden="true"></i> <span>ห้องพัก</span></a></li>
      <li><a href="activity.php"><i class="fa fa-star" aria-hidden="true"></i> <span>กิจกรรม</span></a></li>
      <li class="treeview">
        <a href="#"><i class="fa fa-flag"></i> <span>การจอง</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="booking_calendar.php"><i class="fa fa-list-alt" aria-hidden="true"></i>ปฏิทินกรจอง</a></li>
          <li><a href="booking_list.php"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>รายการจอง</a></li>
        </ul>
      </li>
      <li><a href="report.php"><i class="fa fa-print" aria-hidden="true"></i> <span>รายงาน</span></a></li>
      <li><a href="user.php"><i class="fa fa-users" aria-hidden="true"></i> <span>สมาชิก</span></a></li>
      <!-- <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li> -->

    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>