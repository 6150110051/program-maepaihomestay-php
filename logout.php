<?php
session_start();
// session_destroy คือการล้างหรือการยกเลิก session ที่มีการเก็บเอาไว้ออกไป
session_destroy();
header("Location: index.php");	
?>