<?php

include('header/h_nav.php');
$sql = "SELECT * FROM tb_activity WHERE activity_id  ORDER BY activity_id  DESC";
$result = mysqli_query($con, $sql);
// echo $sql . '<br>';
// print_r($result);

$activity_date = $_POST['activity_date'];
$activity_time = $_POST['activity_time'];

?>
<title>Document</title>

<!-- banner hompage -->
<link rel="stylesheet" href="header/css/owl.carousel.min.css">

<!-- banner hompage -->
<!-- card -->
<link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Roboto:wght@300&display=swap" rel="stylesheet">
<!-- nav -->
<link rel="stylesheet" href="header/css/style.css">
<link rel="stylesheet" href="stylecard.css">



</head>

<body>
    <a name="a"></a>
    <div id="tm-section-1">
        <div class="tm-bg-white ie-container-width-fix-2">
            <div class="container ie-h-align-center-fix">
                <div class="row">

                    <div class="col-xs-12 ml-auto mr-auto ie-container-width-fix">
                        <form method="POST" action="" class="row g-3 avilacilty">
                            <div class="col-md-5 ">
                                <label for="inputCity" class="form-label">วันที่เข้าร่วมกิจกรรม</label>
                                <input type="date" class="form-control" id="inputCity" name="activity_date" value="<?php echo $activity_date; ?>" required>

                            </div>
                            <div class="col-md-5">
                                <label for="inputCity" class="form-label">ช่วงเวลาในการดำเนินกิจกรรม</label>
                                <input class="form-control" list="datalistOptions" id="exampleDataList" placeholder="ช่วงเวลา" name="activity_time" value="<?php echo $activity_time?>">
                                <datalist id="datalistOptions">
                                    <option value="เช้า">
                                    <option value="บ่าย">
                                    <option value="เย็น">
                                </datalist>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-v btn-primary">ตกลง</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container" style="padding-top: 20px;">

        <div class="row">
            <div class="col-md-3" style="padding-top: 50px;">
                <h4>สถานที่ท่องเที่ยวใกล้เคียง</h4>
                <div class="food-card">
                    <!-- <div class="food-card_img">
                        <img src="https://i.imgur.com/eFWRUuR.jpg" alt="">
                        <a href="#!"><i class="far fa-heart"></i></a>
                    </div> -->
                    <div class="food-card_content">

                        <div class="food-card_bottom-section">
                            <div class="space-between">
                                <div>

                                    <!-- <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4>
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4>
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4>
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> 
                                        <h4> วัดราชา</h4> -->
                                </div>

                            </div>

                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-9">
                <h1><a class="food-card_title">Activity</a></h1>
                <?php
                while ($row_prd = mysqli_fetch_array($result)) {
                    // print_r($row_prd );
                ?>
                    <div class="food-card food-card--vertical">
                        <div class="food-card_img">
                            <img src="image/img_activity/<?php echo $row_prd["activity_picture1"]; ?>" alt="">
                            <a href="#!"><i class="fa fa-heart"></i></a>
                        </div>

                        <form method="POST" action="product_detail.php?activity_id=<?php echo $row_prd[0]; ?>">
                            <div class="food-card_content">
                                <div class="food-card_title-section">
                                    <a href="#!" class="food-card_title"><?php echo $row_prd["activity_name"]; ?></a>

                                </div>
                                <div class="food-card_bottom-section">
                                    <div class="space-between">
                                        <div>
                                            <span><i class="fas fa-star"></i></span>
                                            <span><i class="fas fa-star"></i></span>
                                            <span><i class="fas fa-star"></i></span>
                                            <span><i class="fas fa-star"></i></span>
                                            <span><i class="fas fa-star-half-alt"></i></span>
                                        </div>
                                        <div class="pull-right">
                                            <span class="badge badge-success">200 รีวิว</span>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="space-between">
                                        <div class="food-card_price">
                                            <p><span><?php echo "ราคาเริ่มที่ " . $row_prd["activity_price_adult"] . " บาท"; ?></span></p>
                                        </div>
                                        <div class="food-card_order-count">
                                            <div class="input-group">
                                                <div class="input-group-prepend" style="margin-left: 70%;">
                                                    <input type="hidden" name="activity_date" value="<?php echo $_POST['activity_date'];; ?>">
                                                    <input type="hidden" name="activity_time" value="<?php echo $_POST['activity_time'];; ?>">
                                                    <?php if ($activity_date == "" && $activity_time == "") { ?>
                                                        <a href="#a" class='btn btn-outline-secondary' onclick="alert('กรุณาเลือกวันที่เข้าพัก')">ดูรายละเอียด</a>

                                                    <?php } else { ?>

                                                        <button type="submit" class='btn btn-outline-secondary'>ดูรายละเอียด</button>
                                                    <?php } ?>
                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php
                }
                ?>

            </div>
        </div>

    </div>


    <?php
    include('footer/footer.php');
    ?>

</body>

</html>