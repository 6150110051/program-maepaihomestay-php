<?php
include('header/h_nav.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body background="image/aa.png">
  
    <div class="container-fluid" style="padding-top:100px; padding-bottom:100px;">
      <div class="col-md-12 mt-4">
        <div class="row justify-content-md-center">
          <div class="col-md-auto"><?php echo $check_submit; ?></div>
        </div>
      </div>
      <div class="row justify-content-md-center">
        <div class="col-md-5">
          <div class="card border-dark mt-2">
            <center>
              <h5 class="card-header"><i class="fa fa-address-book-o" aria-hidden="true"></i> สมัครสมาชิก <i class="fa fa-address-book-o" aria-hidden="true"></i></h5>
            </center>
            <div class="card-body">
              <div class="row justify-content-md-center mb-2">
                <div class="col col-lg-6">
                  <!-- <img src="assets/images/register.png" style="width: 100%;"> -->
                </div>
              </div>
              <form name="register" action="member_form_add_db.php" method="POST">
                <div class="form-group">
                  <label>ชื่อผู้ใช้</label>
                  <input type="text" class="form-control" name="user_username" placeholder="Enter Username" required="" />
                </div>
                <div class="form-group">
                  <label>รหัสผ่าน</label>
                  <input type="password" class="form-control" name="user_password" placeholder="Enter Password" required="" />
                </div>
                <div class="form-group">
                  <label>ชื่อ</label>
                  <input type="text" class="form-control" name="user_name" placeholder="Enter Name" required="" />
                </div>
                <div class="form-group">
                  <label>นามสกุล</label>
                  <input type="text" class="form-control" name="user_surname" placeholder="Enter Surname" required="" />
                </div>
                <div class="form-group">
                  <label>เพศ</label>
                  <select class="form-control" name="user_sex">
                    <option value="ชาย">ชาย</option>
                    <option value="หญิง">หญิง</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>วันเกิด</label>
                  <input type="date" class="form-control" name="user_birthdate" />
                </div>
                <div class="form-group">
                  <label>เบอร์</label>
                  <input type="text" class="form-control" name="user_phone" placeholder="Enter phone" required="" />
                </div>
                <div class="form-group">
                  <label>ไลน์</label>
                  <input type="text" class="form-control" name="user_line" placeholder="Enter line" required="" />
                </div>
                <div class="form-group">
                  <label>เฟสบุ๊ค</label>
                  <input type="text" class="form-control" name="user_facebook" placeholder="Enter Facebook" required="" />
                </div>
                <div class="form-group">
                  <label>อีเมล์</label>
                  <input type="email" class="form-control" name="user_email" placeholder="Enter Email" required="" />
                </div>
                <center>
                  <button type="submit" class="btn btn-success" name="submit">สมัครสมาชิก</button>
                </center>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
 
</body>

</html>