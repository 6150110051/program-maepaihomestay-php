 <!-- footer -->
 <br><br><br><br>
 <div class="image-hover">
   <div class="box">
     <img src="image/f1.jpeg">

   </div>
   <div class="box">
     <img src="image/f2.jpg">

   </div>
   <div class="box">
     <img src="image/f3.jpg">

   </div>
   <div class="box">
     <img src="image/f4.jpg">

   </div>
 </div>

 <footer class="footer">

   <div class="container">
     <div class="row">
       <div class="footer-col">
         <h4>แม่ไพโฮมสเตย์ล่องแพกอนโดล่า</h4>
         <ul>
           <li><a href="https://1th.me/8S6Uk">แม่ไพโฮมสเตย์ล่องแพกอนโดล่า 34/4 ม.4 ต.บางทอง อ.ท้ายเหมือง จ.พังงา 82120
               <br></a></li>
         </ul>
       </div>
       <div class="footer-col">
         <h4>contact</h4>
         <ul>
           <li><a href="0034752111"> <i class="fas fa-phone icon-color"></i>+66 (034) 752-111 </a></li>
           <li><a href="0824442111"> <i class="fas fa-mobile-alt icon-color"></i>+66 (82) 444-2111</a></li>
           <li><a href="#"> <i class="far fa-envelope icon-color"></i> maepaihomestay@gmail.com</a></li>
         </ul>
       </div>
       <div class="footer-col">
         <h4>follow us</h4>
         <div class="social-links">
           <a href="https://1th.me/yvrKg"><i class="fab fa-facebook-f"></i></a>
           <a href="#"><i class="fab fa-twitter"></i></a>
           <a href="https://1th.me/0fz8r"><i class="fab fa-instagram"></i></a>
           <a href="https://1th.me/wLvxQ"><i class="fab fa-linkedin-in"></i></a>
         </div>
       </div>

       <div class="footer-col">
       <h4>Mape</h4>
             <iframe style="height: 150px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d31578.095956715006!2d98.29322935747756!3d8.375869714429234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3050f9ead1dcbc29%3A0x59015f61280d6e7!2z4LmB4Lih4LmI4LmE4Lie4LmC4Liu4Lih4Liq4LmA4LiV4Lii4LmM!5e0!3m2!1sth!2sth!4v1619529611822!5m2!1sth!2sth" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
       </div>
     </div>
   </div>
   <div class="footer-bottom">
     <p>
       Copyright ©2020 All rights reserved | This web is made with by Maepai
     </p>
   </div>

 </footer>
 <!-- footer -->