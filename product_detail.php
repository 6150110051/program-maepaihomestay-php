<?php
include('header/h_nav.php');
$room_id = $_GET["room_id"];
$user_name = $_SESSION['user_username'];
$activity_id = $_GET["activity_id"];
$activity_date = $_POST['activity_date'];
$activity_time = $_POST['activity_time'];
print_r($_POST);
// echo '<pre>';
// print_r($_SESSION);
// echo '<pre>';

if ($room_id != '') {
    $sqlroom = "SELECT * FROM tb_room  WHERE room_id = $room_id";
    $resultroom = mysqli_query($con, $sqlroom) or die("Error in query: $sqlroom " . mysqli_error($con));
    $rowroom = mysqli_fetch_array($resultroom);
} else {
    $sqlactivity = "SELECT * FROM tb_activity  WHERE activity_id  = $activity_id";
    $resultactivity = mysqli_query($con, $sqlactivity) or die("Error in query: $sqlactivity " . mysqli_error($con));
    $rowactivity = mysqli_fetch_array($resultactivity);
    // echo '<pre>';
    // print_r($rowactivity);
    // echo '<pre>';
}
// echo '<pre>';
// print_r($rowactivity);
// echo '<pre>';

// echo '<pre>';
// print_r($row );
// echo '<pre>';
// exit;
?>
<title>Document</title>

<style>
    @import url("https://fonts.googleapis.com/css2?family=Mansalva&display=swap");
    /* gallery specific CSS */

    .-fx-image-gal {
        display: flex;
        flex-wrap: wrap;
        justify-content: center;
        width: 85%;
        /* arbitrary valye */
        margin: 0px auto;
        padding-top: 100px;
    }

    .-fx-gal-item {
        width: 30%;
        /* for 3 columns */
        margin: 5px;
        overflow: hidden;
        border-radius: 15px;
    }

    .-fx-gal-image-thumb img {
        width: 100%;
        border-radius: 15px;
        cursor: pointer;
        transition: all 0.3s ease;
    }

    .-fx-gal-image-thumb:hover img {
        width: 100%;
        cursor: pointer;
        -webkit-filter: grayscale(0%);
        -moz-filter: grayscale(0%);
        filter: grayscale(0%);

        transform: scale(1.2);
        transition: all 0.5s ease;
    }

    .-fx-gal-image-thumb:focus+.-fx-gal-image-big {
        display: block;
    }

    .-fx-gal-image-big {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        background-color: rgba(5, 10, 15, 0.8);
        overflow: hidden;
        height: 100vh;
        width: 100vw;
        z-index: 999;
        transition: all 0.3s ease;
    }

    .-fx-gal-image-big img {
        max-width: 90vw;
        position: absolute;
        box-shadow: 0px 0px 800px 40px rgba(0, 0, 0, 0.9);
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
    }

    /* gallery specific CSS */


    /* รูปภาพตัวอย่างสินค้า  */
    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }


    .modal {
        display: none;

        position: fixed;

        z-index: 1;

        padding-top: 100px;

        left: 0;
        top: 0;
        width: 100%;

        height: 100%;

        overflow: auto;

        background-color: rgb(0, 0, 0);

        background-color: rgba(0, 0, 0, 0.9);

    }


    .modal-content {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
    }


    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }


    .modal-content,
    #caption {
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @keyframes zoom {
        from {
            transform: scale(0.1)
        }

        to {
            transform: scale(1)
        }
    }


    .close {
        position: sticky;
        top: 15px;
        right: 35px;
        color: #f1f1f1;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: #bbb;
        text-decoration: none;
        cursor: pointer;
    }


    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }

    .apadding {
        padding-right: 20px;
    }

    /* รูปภาพตัวอย่างสินค้า  */
</style>

</head>

<body>
    <!-- room -->
    <?php if ($room_id != '') { ?>
        <div class="-fx-image-gal">
            <div class="-fx-gal-item">
                <div class="-fx-gal-image-thumb" tabindex="1">
                    <img src="image/img_room/<?php echo $rowroom["room_picture1"]; ?>" />
                </div>
                <div class="-fx-gal-image-big">
                    <img src="image/img_room/<?php echo $rowroom["room_picture1"]; ?>" />
                </div>
            </div>

            <div class="-fx-gal-item">
                <div class="-fx-gal-image-thumb" tabindex="1">
                    <img src="image/img_room/<?php echo $rowroom["room_picture1"]; ?>" />
                </div>
                <div class="-fx-gal-image-big">
                    <img src="image/img_room/<?php echo $rowroom["room_picture1"]; ?>" />
                </div>
            </div>

            <div class="-fx-gal-item">
                <div class="-fx-gal-image-thumb" tabindex="1">
                    <img src="image/img_room/<?php echo $rowroom["room_picture1"]; ?>" />
                </div>
                <div class="-fx-gal-image-big">
                    <img src="image/img_room/<?php echo $rowroom["room_picture1"]; ?>" />
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-8" style="margin-right: 80px;">
                            <h5><b><?php echo $rowroom["room_name"]; ?></b></h5>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <h5>
                                        <p>ไม่สามารถยกเลิกได้</p>
                                        <p>ไม่มี Wi-Fi</p>
                                    </h5>
                                </div>
                                <div class="col-md-4">
                                    <h5>
                                        <p>ชำระเงินโดยวางมัดจำ</p>
                                        <p>ที่พักติดริมคลอง</p>
                                    </h5>
                                </div>
                                <div class="col-md-4">
                                    <h5>
                                        <p>จ่ายเงินทั้งสินหน้าเคาเตอร์</p>
                                    </h5>
                                </div>
                            </div>
                            <hr>
                            <pre><?php echo $rowroom["room_details"]; ?></pre>

                            <hr>
                            <div id="myModal" class="modal">
                                <span class="close">&times;</span>
                                <img class="modal-content" id="img01">
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <br>
                            <p>
                                ชื่อห้องพัก : <?php echo $rowroom["room_name"]; ?>
                                <br>
                                ราคาเริ่มต้น : <font color="red"> <?php echo $rowroom["room_price_adult"]; ?> </font> บาท <br>
                            </p>
                            <br> จำนวนการเข้าชม <?php echo $rowroom["p_view"]; ?> ครั้ง
                            <p>
                                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5500ee80057fdb99"></script>
                            <div class="addthis_inline_share_toolbox_sf2w"></div>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8" style="background-color: #e3f2fd; margin-right: 80px; padding:2%;">
                            <form method="POST" action="cart.php?room_id=<?php echo $rowroom['room_id'] ?>&act=add">
                                <h4>ระบุจำนวนผู้เข้าพัก</h4>
                                <div class="row" style="margin:2%; background-color: white; padding:2%;">
                                    <div class="col-md-4">ผู้ใหญ่ราคา/คน</div>
                                    <div class="col-md-4"><?php echo "฿ " . $rowroom["room_price_adult"]; ?></div>
                                    <div class="col-md-2"><input type="number" min="0" name="n_adult" class="form-control" value="" required></div>
                                    <div class="col-md-2">คน</div>
                                </div>
                                <div class="row" style="margin:2%; background-color: white; padding:2%;">
                                    <div class="col-md-4">เด็กราคา/คน(อายุ 4-11 ขวบ)</div>
                                    <div class="col-md-4"><?php echo "฿ " .  $rowroom["room_price_child"]; ?></div>
                                    <div class="col-md-2"><input type="number" min="0" name="n_child" class="form-control" value="0"></div>
                                    <div class="col-md-2">คน</div>
                                </div>
                                <input type="hidden" name="date_start" value="<?php echo $_POST['date_start'];; ?>">
                                <input type="hidden" name="date_end" value="<?php echo $_POST['date_end'];; ?>">
                                <!-- เมื่อกดจะไปหน้า cart  -->
                                <?php if ($user_name == "") { ?>
                                    <a class="btn btn-info btn-sm" onclick="alert('กรุณาเข้าสู่ระบบ')" style="color: #f1f1f1;"> หยิบใส่ตะกร้า</a>
                                <?php  } else { ?>
                                    <button class="btn btn-info btn-sm" type="submit" value="Add to Cart"> หยิบใส่ตะกร้า</button>
                                <?php  } ?>
                                <!-- <button class="btn btn-danger btn-sm" type="submit" value="Add to Cart"> จองตอนนี้</button> -->
                            </form>
                        </div>

                        <div class="col-md-3 " style="background-color: #e3f2fd; ">
                            <br>
                            <p><strong>รายละเอียดแพ็กเกจที่เลือก</strong></p>
                            <div class="row" style="background-color: white; border:rgba(0, 0, 0, 0.9) solid; padding:2%; margin:2%">
                                <p>ยืนยันการจองภายใน 24 ชม.</p>
                                <p>ไม่สามารถยกเลิกเมื่อชำระเงิน</p>
                                <p>เปลี่ยนวันเวลา กรุณาติดต่อโฮมสเตย์</p>
                            </div>
                            <div class="row" style=" padding:2%; margin:2%">
                                <p><strong>รายละเอียดสำคัญ</strong></p>
                                &#177; <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- closs room -->
    <?php } else { ?>
        <!-- activity -->
        <div class="-fx-image-gal">
            <div class="-fx-gal-item">
                <div class="-fx-gal-image-thumb" tabindex="1">
                    <img src="image/img_activity/<?php echo $rowactivity["activity_picture1"]; ?>" />
                </div>
                <div class="-fx-gal-image-big">
                    <img src="image/img_activity/<?php echo $rowactivity["activity_picture1"]; ?>" />
                </div>
            </div>
            <div class="-fx-gal-item">
                <div class="-fx-gal-image-thumb" tabindex="1">
                    <img src="image/img_activity/<?php echo $rowactivity["activity_picture1"]; ?>" />
                </div>
                <div class="-fx-gal-image-big">
                    <img src="image/img_activity/<?php echo $rowactivity["activity_picture1"]; ?>" />
                </div>
            </div>

            <div class="-fx-gal-item">
                <div class="-fx-gal-image-thumb" tabindex="1">
                    <img src="image/img_activity/<?php echo $rowactivity["activity_picture1"]; ?>" />
                </div>
                <div class="-fx-gal-image-big">
                    <img src="image/img_activity/<?php echo $rowactivity["activity_picture1"]; ?>" />
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="row">
                        <div class="col-md-8" style="margin-right: 80px;">
                            <h5><b><?php echo $rowactivity["activity_name"]; ?></b></h5>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <h5>
                                        <p>ไม่สามารถยกเลิกได้</p>
                                        <p>ไม่มี Wi-Fi</p>
                                    </h5>
                                </div>
                                <div class="col-md-4">
                                    <h5>
                                        <p>ชำระเงินโดยวางมัดจำ</p>
                                        <p>ที่พักติดริมคลอง</p>
                                    </h5>
                                </div>
                                <div class="col-md-4">
                                    <h5>
                                        <p>จ่ายเงินทั้งสินหน้าเคาเตอร์</p>
                                    </h5>
                                </div>
                            </div>
                            <hr>
                            <pre> <?php echo $rowactivity["activity_details"]; ?></pre>
                            <hr>
                            <div id="myModal" class="modal">
                                <span class="close">&times;</span>
                                <img class="modal-content" id="img01">
                                <div id="caption"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <br>
                            <p>
                                ชื่อห้องพัก : <?php echo $rowactivity["activity_name"]; ?>
                                <br>
                                ราคาเริ่มต้น : <font color="red"> <?php echo $rowactivity["activity_price_adult"]; ?> </font> บาท <br>

                                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5500ee80057fdb99"></script>
                            <div class="addthis_inline_share_toolbox_sf2w"></div>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8" style="background-color: #e3f2fd; margin-right: 80px; padding:2%;">
                            <form method="POST" action="cart.php?activity_id=<?php echo $rowactivity['activity_id'] ?>&act=add">
                                <h4>ระบุจำนวนผู้เข้าพัก</h4>
                                <div class="row" style="margin:2%; background-color: white; padding:2%;">
                                    <div class="col-md-4">ผู้ใหญ่ราคา/คน</div>
                                    <div class="col-md-4"><?php echo "฿ " . $rowactivity["activity_price_adult"]; ?></div>
                                    <div class="col-md-2"><input type="number" min="0" name="n_adult" class="form-control" value="" required></div>
                                    <div class="col-md-2">คน</div>
                                </div>
                                <div class="row" style="margin:2%; background-color: white; padding:2%;">
                                    <div class="col-md-4">เด็กราคา/คน(อายุ 4-11 ขวบ)</div>
                                    <div class="col-md-4"><?php echo "฿ " .  $rowactivity["activity_price_child"]; ?></div>
                                    <div class="col-md-2"><input type="number" min="0" name="n_child" class="form-control" value="0"></div>
                                    <div class="col-md-2">คน</div>
                                </div>
                                <input type="hidden" name="date_start" value="<?php echo $_POST['date_start']; ?>">
                                <input type="hidden" name="date_end" value="<?php echo $_POST['date_end']; ?>">
                                <input type="hidden" name="activity_date" value="<?php echo $_POST['activity_date']; ?>">
                                <input type="hidden" name="activity_time" value="<?php echo $_POST['activity_time']; ?>">
                                <!-- เมื่อกดจะไปหน้า cart  -->
                                <?php if ($user_name == "") { ?>
                                    <a class="btn btn-info btn-sm" onclick="alert('กรุณาเข้าสู่ระบบ')" style="color: #f1f1f1;"> หยิบใส่ตะกร้า</a>
                                <?php  } else { ?>
                                    <button class="btn btn-info btn-sm" type="submit" value="Add to Cart"> หยิบใส่ตะกร้า</button>
                                <?php  } ?>
                                <!-- <button class="btn btn-danger btn-sm" type="submit" value="Add to Cart"> จองตอนนี้</button> -->
                            </form>
                        </div>

                        <div class="col-md-3 " style="background-color: #e3f2fd; ">
                            <br>
                            <p><strong>รายละเอียดแพ็กเกจที่เลือก</strong></p>
                            <div class="row" style="background-color: white; border:rgba(0, 0, 0, 0.9) solid; padding:2%; margin:2%">
                                <p>ยืนยันการจองภายใน 24 ชม.</p>
                                <p>ไม่สามารถยกเลิกเมื่อชำระเงิน</p>
                                <p>เปลี่ยนวันเวลา กรุณาติดต่อโฮมสเตย์</p>
                            </div>
                            <div class="row" style=" padding:2%; margin:2%">
                                <p><strong>รายละเอียดสำคัญ</strong></p>
                                &#177; <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- closs activity -->


    <!-- ใชสำหรับการโชว์ภาพขึ้นมา -->
    <script>
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var img = document.getElementById('myImg');
        var modalImg = document.getElementById("img01");
        var captionText = document.getElementById("caption");
        img.onclick = function() {
            modal.style.display = "block";
            modalImg.src = this.src;
            captionText.innerHTML = this.alt;
        }

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }
    </script>

    <?php
    include('footer/footer.php');
    ?>
</body>

</html>