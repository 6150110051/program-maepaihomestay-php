<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
    header("location:../login.php");
    exit();
} elseif ($_SESSION["user_level"] != "admin") {
    header("location:../index.php");
    exit();
}

$strSQL = "SELECT * FROM tb_user WHERE user_username = '" . $_SESSION['user_username'] . "'";
$objQuery = mysqli_query($Connection, $strSQL);
$objResult = mysqli_fetch_array($objQuery, MYSQLI_ASSOC);

$check_submit = "";
if (isset($_GET['update'])) {
    if ($_GET['update'] == "pass") {
        $check_submit = '<div class="alert alert-success" role="alert">';
        $check_submit .= '<span><i class="fa fa-check"></i> บันทึกข้อมูลเรียบร้อยแล้ว</span>';
        $check_submit .= '</div>';
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
</head>

<body class="skin-blue">
    <div class="wrapper">
        <?php include '../includes/navbar_admin.php'; ?>
        <div class="content-wrapper">

            <div class="container-fluid">
                
                    <div class="row justify-content-md-center ">
                        <div class="col-md-auto mt-3"><?php echo $check_submit; ?></div>
                    </div>
                
                <div class="row justify-content-md-center">
                    <div class="col-md-5">
                        <div class="card border-dark ">
                            <center>
                            <h5 class="card-header"><i class="fa fa-address-card fa-lg"></i> ข้อมูลส่วนตัวของฉัน</h5>
                            </center>
                            <div class="card-body">
                                <h4>ชื่อผู้ใช้ : <span class="badge badge-info"><?php echo $objResult["user_username"]; ?></span></h4>
                                <h4>ชื่อ - นามสกุล : <span class="badge badge-info"><?php echo $objResult["user_name"] . " " . $objResult["user_surname"]; ?></span></h4>
                                <h4>เพศ : <span class="badge badge-info"><?php echo $objResult["user_sex"]; ?></span></h4>

                                <h4>วันเกิด : <span class="badge badge-info"><?php echo $objResult["user_birthdate"]; ?></span></h4>
                                <h4>เบอร์โทรศัพท์ : <span class="badge badge-info"><?php echo $objResult["user_phone"]; ?></span></h4>
                                <h4>ไลน์ : <span class="badge badge-info"><?php echo $objResult["user_line"]; ?></span></h4>
                                <h4>เฟสบุ๊ค : <span class="badge badge-info"><?php echo $objResult["user_facebook"]; ?></span></h4>
                                <h4>อีเมล์ : <span class="badge badge-info"><?php echo $objResult["user_email"]; ?></span></h4>
                                
                                <h4>ระดับผู้ใช้ : <span class="badge badge-info"><?php if ($objResult["user_level"] == "member") {
                                                                                        echo "สมาชิก";
                                                                                    } else {
                                                                                        echo "ผู้ดูแลระบบ";
                                                                                    } ?></span></h4>

                                <hr>
                                
                                <div class="mt-3">
                                    <center>
                                    <a href="profile_admin_edit.php" class="btn btn-success">แก้ไขข้อมูล</a>
                                    <a href="profile_admin_pass.php" class="btn btn-warning">เปลี่ยนรหัสผ่าน</a>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br>
            </div>
        </div>
    </div>
    
    <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
    <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>
    <?php mysqli_close($Connection); ?>
</body>

</html>