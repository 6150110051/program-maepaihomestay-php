<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
  header("location:../login.php");
  exit();
} elseif ($_SESSION["user_level"] != "admin") {
  header("location:../index.php");
  exit();
}

//รับค่าไอดีมา
$id = $_GET["id"];

//แสดงข้อมูลเฉพาะไอดีที่รับมา
$sql = "SELECT * FROM tb_user WHERE user_id = '" . $id . "'";
$query = mysqli_query($Connection, $sql);
$result = mysqli_fetch_array($query, MYSQLI_ASSOC);

if (isset($_POST["submit"])) {
  $sql = "DELETE FROM tb_user WHERE user_id = '" . $id . "' ";
  $query = mysqli_query($Connection, $sql);

  header("location:user.php?delete=pass");
  exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $title; ?></title>
  <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
  <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">

</head>

<body class="skin-blue">

  <div class="wrapper">
    <?php include '../includes/navbar_admin.php'; ?>
    <div class="content-wrapper">
      <!-- ต้องมี3บรรทัดนี้ทุกหน้า Admin -->

      <div class="container-fluid">
        <div class="row justify-content-md-center">

          <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <div class="row justify-content-md-center">
              <div class="col-6"><br>
                <div class="card">
                  <center>
                    <h5 class="card-header ">ลบข้อมูลสมาชิก <?php echo ' ID : ' . $result['user_id']; ?></h5>
                  </center>
                  <div class="card-body">

                    <!-- ลบข้อมูลสมาชิก -->
                    <form method="post">
                      <div class="mb-3">
                        <h5>สมาชิก <?php echo ' ชื่อผู้ใช้ : ' . $result['user_username']; ?></h5>
                      </div>
                      <button type="submit" name="submit" class="btn btn-primary">ยืนยัน</button>
                      <button type="button" class="btn btn-secondary" onclick="window.location.href='user.php'">ยกเลิก</button>
                    </form>

                  </div>
                </div>
              </div>
            </div><br>

          </main>
        </div>



      </div>
    </div>
  </div>



  <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
  <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
  <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

  <?php mysqli_close($Connection); ?>
</body>

</html>