<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
    header("location:login.php");
    exit();
} elseif ($_SESSION["user_level"] != "admin") {
    header("location:../index.php");
    exit();
}

$sql = "SELECT * FROM tb_user WHERE user_username = '" . $_SESSION['user_username'] . "'";
$query = mysqli_query($Connection, $sql);
$result = mysqli_fetch_array($query);

if (isset($_POST["save"])) {
    $sql_2 = "UPDATE tb_user SET user_name = '" . $_POST["user_name"] . "' , user_surname = '" . $_POST["user_surname"] . "' , user_sex = '" . $_POST["user_sex"] . "' , user_birthdate = '" . $_POST["user_birthdate"] . "' , user_phone = '" . $_POST["user_phone"] . "' , user_line = '" . $_POST["user_line"] . "' , user_facebook = '" . $_POST["user_facebook"] . "' , user_email = '" . $_POST["user_email"] . "' WHERE user_username = '" . $_SESSION['user_username'] . "'";
    $query_2 = mysqli_query($Connection, $sql_2);

    header("location:profile_admin.php?update=pass");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">

</head>

<body class="skin-blue">
    <div class="wrapper">
        <?php include '../includes/navbar_admin.php'; ?>
        <div class="content-wrapper">

            <div class="container-fluid">
                <div class="row justify-content-md-center">
                    <div class="col-md-5 mb-4">

                        <div class="card border-dark mt-2">
                            <h5 class="card-header">แก้ไขข้อมูลผู้ใช้ ID : <?php echo $result[0]; ?></h5>
                            <div class="card-body">
                                <div class="row justify-content-md-center mb-2">
                                    <div class="col col-lg-6">
                                        <!-- <img src="images/register.png" style="width: 100%;"> -->
                                    </div>
                                </div>
                                <form method="post">
                                    <div class="mb-3">
                                        <label class="form-label">ชื่อผู้ใช้</label>
                                        <input type="text" class="form-control" value="<?php echo $result["user_username"]; ?>" disabled />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">ชื่อ</label>
                                        <input type="text" class="form-control" name="user_name" value="<?php echo $result["user_name"]; ?>" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">นามสกุล</label>
                                        <input type="text" class="form-control" name="user_surname" value="<?php echo $result["user_surname"]; ?>" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">เพศ</label>
                                        <select class="form-select" name="user_sex">
                                            <option value="ชาย" <?php if ($result["user_sex"] == 'ชาย') {
                                                                    echo " selected";
                                                                } ?>>ชาย</option>
                                            <option value="หญิง" <?php if ($result["user_sex"] == 'หญิง') {
                                                                        echo " selected";
                                                                    } ?>>หญิง</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">วันเกิด</label>
                                        <input type="date" class="form-control" name="user_birthdate" value="<?php echo $result["user_birthdate"]; ?>" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">เบอร์</label>
                                        <input type="text" class="form-control" name="user_phone" value="<?php echo $result["user_phone"]; ?>" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">ไลน์</label>
                                        <input type="text" class="form-control" name="user_line" value="<?php echo $result["user_line"]; ?>" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">เฟสบุ๊ค</label>
                                        <input type="text" class="form-control" name="user_facebook" value="<?php echo $result["user_facebook"]; ?>" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">อีเมล์</label>
                                        <input type="email" class="form-control" name="user_email" value="<?php echo $result["user_email"]; ?>" required />
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" onclick="window.location.href='profile_admin.php'">ยกเลิก</button>
                                    <button type="submit" class="btn btn-success" name="save">บันทึกข้อมูล</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
    <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

    <?php mysqli_close($Connection); ?>
</body>

</html>