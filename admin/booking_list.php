<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
  header("location:../login.php");
  exit();
} elseif ($_SESSION["user_level"] != "admin") {
  header("location:../index.php");
  exit();
}

//ใช้เพื่อแสดงตารางเป็นลูป
$num = 1;

$sql = "SELECT * FROM tb_booking ORDER BY book_id ASC";
$query = mysqli_query($Connection, $sql);

if (isset($_GET["submit"])) {
  if ($_GET["submit"] == "pass") {
      $check_submit = check_submit_p2("ส่งเมลล์เรียบร้อยแล้ว");
  }
}

if (isset($_GET["update"])) {
  if ($_GET["update"] == "pass") {
    $check_submit = check_submit_p2("บันทึกข้อมูลเรียบร้อยแล้ว");
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $title; ?></title>
  <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
  <!-- ลิงค์เรียกใช้ Data Table -->
  <link rel="stylesheet" type="text/css" href="../assets/DataTables/datatables.min.css" />

</head>

<body class="skin-blue">
  <div class="wrapper">
    <?php include '../includes/navbar_admin.php'; ?>
    <div class="content-wrapper">

      <div class="container-fluid">
        <div class="row justify-content-md-center">
          <div class="col-md-11 mt-4 mb-4">

            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
              <h1 class="h2">รายการจอง</h1>
            </div>
            <?php echo $check_submit; ?>

            <!-- แสดงข้อมูลเป็นตาราง -->
            <div class="card mt-3">
              <div class="card-body">
                <table class="table table-hover table-bordered mb-0" id="datatables">
                  <thead>
                    <tr class="bg-info">
                      <!-- <th scope="col" width="60px">ลำดับที่</th> -->
                      <th scope="col">รหัสการจอง</th>
                      <th scope="col">ชื่อผู้จอง</th>                
                      <th scope="col">เบอร์โทร</th>
                      <th scope="col">อีเมลล์</th>
                      <th scope="col" width="90px">วันที่จอง</th>
                      <th scope="col">ค่ามัดจำ</th>
                      <th scope="col">สลิปค่ามัดจำ</th>
                      <!-- <th scope="col">ราคาทั้งหมด</th> -->
                      <th scope="col">สถานะการจอง</th>
                      <th scope="col" width="60px">ตัวเลือก</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    while ($result = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
                    ?>
                      <tr>
                        <!-- <th scope="row"><?php //echo $num; ?></th> -->
                        <td><?php echo $result["book_id"]; ?></td>
                        <td><?php echo $result["book_name"]; ?></td>    
                        <td><?php echo $result["book_phone"]; ?></td>
                        <td><?php echo $result["book_email"]; ?></td>
                        <td><?php echo $result["book_date"]; ?></td>
                        <td><?php echo $result["book_desposit"]; ?></td>
                        <td><img src="../imgslip/<?php echo $result["book_slip"];?>" width="100px"></td>
                        <!-- <td><?php //echo $result["book_total"]; ?></td> -->
                        <td><span class="badge badge-info"><?php
                                                            if ($result["book_status"] == 1) {
                                                              echo "รออนุมัติ";
                                                            } else if ($result["book_status"] == 2) {
                                                              echo "อนุมัติ(รอชำระเงิน)";
                                                            } else if ($result["book_status"] == 3) {
                                                              echo "จองสำเร็จ";
                                                            } else if ($result["book_status"] == 4) {
                                                              echo "ยกเลิก";
                                                            } ?></span></td>
                        <td>
                          <!-- ปุ่มแก้ไข -->
                          <button type="button" class="btn btn-success btn-sm" onclick="window.location.href='booking_edit.php?id=<?php echo $result['book_id']; ?>'"><i class="fa fa-edit"></i></button>
                          <!-- ส่งอีเมลล์-->
                          <button type="button" class="btn btn-info btn-sm" onclick="window.location.href='booking_mail.php?id=<?php echo $result['book_id']; ?>'"><i class="fa fa-envelope"></i></button>
                        </td>
                      </tr>
                    <?php
                      $num++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>

  <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
  <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
  <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#datatables').DataTable();
    });
  </script>
  <script type="text/javascript" src="../assets/DataTables/datatables.min.js"></script>

  <!-- <script src="../assets/js/bootstrap.bundle.min.js"></script> -->
  <?php mysqli_close($Connection); ?>
</body>

</html>