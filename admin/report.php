<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
    header("location:../login.php");
    exit();
} elseif ($_SESSION["user_level"] != "admin") {
    header("location:../index.php");
    exit();
}

require_once __DIR__ . '/../vendor/autoload.php';

$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$mpdf = new \Mpdf\Mpdf([
    'fontDir' => array_merge($fontDirs, [
        __DIR__ . '/tmp',
    ]),
    'fontdata' => $fontData + [
        'saraban' => [
            'R' => 'THSarabunNew.ttf',
            'I' => 'THSarabunNew Italic.ttf',
            'B' => 'THSarabunNew Bold.ttf',
            'BI' => 'THSarabunNew BoldItalic.ttf'
        ]
    ],
    'default_font' => 'saraban'
]);


$sql2 = "SELECT * FROM bookingroomdetail ORDER BY room_dt_id ASC";
$query2 = mysqli_query($Connection, $sql2);

// $content = "";
if (mysqli_num_rows($query2) > 0) {
    $total = 0.0;
    $tablebody1="";
    $tablebody2="";
    while ($row = mysqli_fetch_assoc($query2)) {
        $r_price = $row['r_price'];
        $total += $r_price;

        $tablebody1 .= '<tr style="border:1px solid #000;">
        <td style="border-right:1px solid #000; padding:3px; text-align:center;">' . $row['book_id'] . '</td>
        <td style="border-right:1px solid #000; padding:3px; text-align:center;">' . $row['room_id'] . '</td>
        <td style="border-right:1px solid #000; padding:3px; text-align:center;">' . $row['r_check_in'] . '</td>
        <td style="border-right:1px solid #000; padding:3px; text-align:center;">' . $row['r_check__out'] . '</td>
        <td style="border-right:1px solid #000; padding:3px; text-align:center;">' . $row['r_price'] . '</td> 
        </tr>';
    }
    $tablebody2 .= '<tr style="border:1px solid #000;">
        <td colspan="4" style="border-right:1px solid #000; padding:3px; text-align:center;">ยอดรวม</td>
        <td style="border-right:1px solid #000; padding:3px; text-align:center;">' . number_format($total, 2) . '</td>  
        </tr>';
}

$tableh = '
<style>
    body{
        font-family: "THSarabunNew";
    }
</style>
<p style="text-align:center;"> <img src="../images/braner.png" alt="" width="200px"> </p>
<h2 style="text-align:center;"> รายงานสรุปการเข้าพัก </h2>
<h3> รายละเอียด </h3>
<table Width="100%" style="border-collapse:collapse; font-size:16px; margin-top:8px">
        <tr style="border:1px solid #000; padding:4px;">
            <td style="border-right:1px solid #000; padding:3px; text-align:center;"><b>รหัสการจอง</b></td>
            <td style="border-right:1px solid #000; padding:3px; text-align:center;"><b>รหัสห้องพัก</b></td>
            <td style="border-right:1px solid #000; padding:3px; text-align:center;"><b>วันที่เข้าพัก</b></td>
            <td style="border-right:1px solid #000; padding:3px; text-align:center;"><b>วันที่ออก</b></td>
            <td style="border-right:1px solid #000; padding:3px; text-align:center;"><b>จำนวนเงิน</b></td>
        </tr>

    </thead>
    <tbody>';

$tableend = "</tbody>
</table>";

$mpdf->WriteHTML($tableh);

$mpdf->WriteHTML($tablebody1);
$mpdf->WriteHTML($tablebody2);

$mpdf->WriteHTML($tableend);

$mpdf->Output("report_pdf.pdf");
 
// ob_start();
// $html = ob_get_contents();
// $mpdf->WriteHTML($html);
// $mpdf->Output("report_pdf.pfd");
// ob_end_flush();

// if($act=='m'){
//   include('report_m.php');
// } else if($act=='y'){
// }

//เช็คว่าเพิ่มสมาชิกสำเร็จมั้ย ถ้าสำเร็จ"บันทึกข้อมูลเรียบร้อยแล้ว"
// if (isset($_GET["add"])) {
//   if ($_GET["add"] == "pass") {
//     $check_submit = check_submit_p2("บันทึกข้อมูลเรียบร้อยแล้ว");
//   }
// }
// //เช็คว่าแก้ไขสมาชิกสำเร็จมั้ย ถ้าสำเร็จ"บันทึกข้อมูลเรียบร้อยแล้ว"
// if (isset($_GET["update"])) {
//   if ($_GET["update"] == "pass") {
//     $check_submit = check_submit_p2("บันทึกข้อมูลเรียบร้อยแล้ว");
//   }
// }
// //เช็คว่าลบสมาชิกสำเร็จมั้ย ถ้าสำเร็จ"บันทึกข้อมูลเรียบร้อยแล้ว"
// if (isset($_GET["delete"])) {
//   if ($_GET["delete"] == "pass") {
//     $check_submit = check_submit_p2("ลบข้อมูลออกจากระบบเรียบร้อยแล้ว");
//   }
// }


//ใช้เพื่อแสดงตารางเป็นลูป
$num = 1;

$sql = "SELECT * FROM bookingroomdetail ORDER BY room_dt_id ASC";
$query = mysqli_query($Connection, $sql);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
    <!-- ลิงค์เรียกใช้ Data Table -->
    <link rel="stylesheet" type="text/css" href="../assets/DataTables/datatables.min.css" />

</head>

<body class="skin-blue">
    <div class="wrapper">
        <?php include '../includes/navbar_admin.php'; ?>
        <div class="content-wrapper">

            <div class="container-fluid">
                <div class="row justify-content-md-center">
                    <div class="col-md-11 mt-4 mb-4">

                        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
                            <h1 class="h2">
                                รายงาน
                                <small>สรุปการเข้าพัก</small>
                            </h1>

                            <div class="btn-toolbar mb-2 mb-md-2">
                                <!-- กดเลือก -->
                                <a href="report.php?act=m" class="btn btn-success ">รายเดือน</a>&nbsp;

                                <a href="report.php?act=y" class="btn btn-success">รายปี</a>&nbsp;

                                <a href="report_pdf.pdf" class="btn btn-success">ออกรายงาน PDF</a>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary" value="ห้องพัก" onclick="window.location.href='report.php'">ห้องพัก</button>
                        <button type="button" class="btn btn-primary" value="กิจกรรม" onclick="window.location.href='report_activity.php'">กิจกรรม</button>

                        <?php echo $check_submit; ?>

                        <!-- แสดงข้อมูลเป็นตาราง -->
                        <div class="card mt-3">
                            <div class="card-body">

                                <table class="table table-hover table-bordered mb-0" id="datatables">
                                    <thead>
                                        <tr class="bg-info">
                                            <th scope="col" width="60px">ลำดับที่</th>
                                            <th scope="col">รหัสการจอง</th>
                                            <th scope="col">รหัสห้องพัก</th>
                                            <th scope="col">วันที่เข้าพัก</th>
                                            <th scope="col">วันที่ออก</th>
                                            <th scope="col">จำนวนเงิน</th>
                                            <th scope="col" width="60px">ตัวเลือก</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        while ($result = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
                                        ?>
                                            <tr>
                                                <th scope="row"><?php echo $num; ?></th>
                                                <!-- <td><?php //echo $result["r_check_in"]; 
                                                            ?></td>-->
                                                <td><?php echo $result["book_id"]; ?></td>
                                                <td><?php echo $result["room_id"]; ?></td>
                                                <td><?php echo $result["r_check_in"]; ?></td>
                                                <td><?php echo $result["r_check__out"]; ?></td>
                                                <td><?php echo $result["r_price"]; ?></td>
                                                <td>
                                                    <!-- ปุ่มแก้ไข -->
                                                    <center><button type="button" class="btn btn-warning " onclick="window.location.href=''"><i class="fa fa-print"></i></button></center>
                                                </td>
                                            </tr>
                                        <?php
                                            $num++;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>




    <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
    <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatables').DataTable();
        });
    </script>
    <script type="text/javascript" src="../assets/DataTables/datatables.min.js"></script>

    <?php mysqli_close($Connection); ?>
</body>

</html>