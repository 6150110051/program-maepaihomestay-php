<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
    header("location:../login.php");
    exit();
} elseif ($_SESSION["user_level"] != "admin") {
    header("location:../index.php");
    exit();
}

//รับค่าไอดีมา
$id = $_GET["id"];

//แสดงข้อมูลเฉพาะไอดีที่รับมา
$sql = "SELECT * FROM tb_activity WHERE activity_id = '" . $id . "'";
$query = mysqli_query($Connection, $sql);
$result = mysqli_fetch_array($query, MYSQLI_ASSOC);


if (isset($_POST["submit"])) {

    // $activity_id = mysqli_real_escape_string($Connection, $_POST["activity_id"]);
    // $activity_code = mysqli_real_escape_string($Connection, $_POST["activity_code"]);
    // $activity_name = mysqli_real_escape_string($Connection, $_POST["activity_name"]);
    // $activity_price_adult = mysqli_real_escape_string($Connection, $_POST["activity_price_adult"]);
    // $activity_price_child = mysqli_real_escape_string($Connection, $_POST["activity_price_child"]);
    // $activity_details = mysqli_real_escape_string($Connection, $_POST["activity_details"]);
    $activity_picture2 = mysqli_real_escape_string($Connection, $_POST["activity_picture2"]);

    //ฟังก์ชั่นวันที่
    $date = date("Ymd_Hit");
    //ฟังก์ชั่นสุ่มตัวเลข
    $numrand = (mt_rand());


    // อัพโหลดรูปภาพ
    $activity_picture = (isset($_POST['activity_picture']) ? $_POST['activity_picture'] : '');
    $upload = $_FILES['activity_picture']['name'];
    if ($upload != '') {
        //ไฟล์ที่เก็บภาพ
        $path = "../image/img_activity/";
        //ตัดชื่อนามสกุลภาพออกจากกัน
        $type = strrchr($_FILES['activity_picture']['name'], ".");
        //ตั้งชื่อไฟล์ใหม่ สุ่มตัวเลข+วันที่
        $newname = $numrand . $date . $type;
        $path_copy = $path . $newname;
        $path_link = "../image/img_activity/" . $newname;
        //คัดลอกไปยังโฟลเดอร์
        move_uploaded_file($_FILES['activity_picture']['tmp_name'], $path_copy);
    } else {
        //ถ้าไม่มีการแก้ไขภาพ ใช้ภาพเดิม
        $newname = $activity_picture2;
    }


    //อัพเดทข้อมูล
    $sql_2 = "UPDATE tb_activity SET activity_name = '" . $_POST['activity_name'] . "', activity_price_adult = '" . $_POST['activity_price_adult'] . "',
    activity_price_child = '" . $_POST['activity_price_child'] . "', activity_picture1 = '" . $newname . "', activity_time = '" . $_POST['activity_time']. "', activity_details = '"  . $_POST['activity_details'] . "' WHERE activity_id = '" . $id . "'";
    $query_2 = mysqli_query($Connection, $sql_2);

    header("location:activity.php?update=pass");
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">

</head>

<body class="skin-blue">

    <div class="wrapper">
        <?php include '../includes/navbar_admin.php'; ?>
        <div class="content-wrapper">
            <!-- ต้องมี3บรรทัดนี้ทุกหน้า Admin -->

            <div class="container-fluid">
                <div class="row justify-content-md-center">

                    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                        <div class="row justify-content-md-center">
                            <div class="col-6"><br>

                                <div class="row justify-content-md-center">
                                    <div class="col-md-auto"><?php echo $check_submit; ?></div>
                                </div>

                                <div class="card">
                                    <h5 class="card-header">แก้ไขข้อมูลห้องพัก <?php echo ' ID : ' . $result['activity_id']; ?></h5>

                                    <!-- ฟรอมในการแก้ไขข้อมูลสมาชิก -->
                                    <div class="card-body">
                                        <form method="post" enctype="multipart/form-data">

                                            <div class="form-group">
                                                <label>ชื่อห้องพัก</label>
                                                <input type="text" class="form-control" name="activity_name" value="<?php echo $result['activity_name']; ?>" required />
                                            </div>
                                            <div class="form-group">
                                                <label>ราคาสำหรับผู้ใหญ่</label>
                                                <input type="number" class="form-control" name="activity_price_adult" value="<?php echo $result['activity_price_adult']; ?>" required />
                                            </div>
                                            <div class="form-group">
                                                <label>ราคาสำหรับเด็ก</label>
                                                <input type="number" class="form-control" name="activity_price_child" value="<?php echo $result['activity_price_child']; ?>" required />
                                            </div>
                                            <div class="form-group">
                                                รูปภาพเก่า <br>
                                                <img src="../image/img_activity/<?php echo $result['activity_picture1']; ?>" width="300px" alt="">
                                                <br>
                                                <label>รูปภาพ</label>
                                                <input type="file" name="activity_picture" class="form-control" accept="image/*" onchange="readURL(this);">
                                                <img id="blah" src="#" alt="" width="250" class="img-rounded" / style="margin-top: 10px;">
                                            </div>
                                            <div class="form-group">
                                                <label>วันที่เข้ากิจกรรม</label>

                                                <select class="form-select" name="activity_time">
                                                    <option value="รอบเช้า" <?php if ($result["activity_time"] == 'รอบเช้า') {
                                                                                echo " selected";
                                                                            } ?>>รอบเช้า</option>
                                                    <option value="รอบบ่าย" <?php if ($result["activity_time"] == 'รอบบ่าย') {
                                                                                echo " selected";
                                                                            } ?>>รอบบ่าย</option>
                                                    <option value="รอบเย็น" <?php if ($result["activity_time"] == 'รอบเย็น') {
                                                                                echo " selected";
                                                                            } ?>>รอบเย็น</option>
                                                </select>

                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    รายละเอียด
                                                </label>
                                                <div>
                                                <textarea class="form-control" name="activity_details" required style="height: 300px;"><?php echo $result['activity_details']; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <!-- ใว้ใช้ถ้าไม่มีการเปลี่ยนภาพ ภาพเก่าจะไม่หาย -->
                                                <input type="hidden" name="activity_picture2" value="<?php echo $result['activity_picture']; ?>">
                                                <input type="hidden" name="activity_id" value="<?php echo $activity_id; ?>" />
                                                <button type="button" class="btn btn-secondary" onclick="window.location.href='activity.php'">ยกเลิก</button>
                                                <button type="submit" class="btn btn-primary" name="submit">บันทึกข้อมูล</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div><br>
                    </main>

                </div>
            </div>

        </div>
    </div>

    <!-- สคริปเกี่ยวกับภาพ -->
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
    <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

    <?php mysqli_close($Connection); ?>
</body>

</html>