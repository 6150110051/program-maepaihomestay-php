<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
    header("location:../login.php");
    exit();
} elseif ($_SESSION["user_level"] != "admin") {
    header("location:../index.php");
    exit();
}

//รับค่าไอดีมา
$id = $_GET["id"];

//แสดงข้อมูลเฉพาะไอดีที่รับมา
$sql = "SELECT * FROM tb_room WHERE room_id = '" . $id . "'";
$query = mysqli_query($Connection, $sql);
$result = mysqli_fetch_array($query, MYSQLI_ASSOC);


if (isset($_POST["submit"])) {

    // $room_id = mysqli_real_escape_string($Connection, $_POST["room_id"]);
    // $room_code = mysqli_real_escape_string($Connection, $_POST["room_code"]);
    // $room_name = mysqli_real_escape_string($Connection, $_POST["room_name"]);
    // $room_price_adult = mysqli_real_escape_string($Connection, $_POST["room_price_adult"]);
    // $room_price_child = mysqli_real_escape_string($Connection, $_POST["room_price_child"]);
    // $room_details = mysqli_real_escape_string($Connection, $_POST["room_details"]);
    $room_picture2 = mysqli_real_escape_string($Connection, $_POST["room_picture2"]);

    //ฟังก์ชั่นวันที่
    $date = date("Ymd_Hit");
    //ฟังก์ชั่นสุ่มตัวเลข
    $numrand = (mt_rand());


    // อัพโหลดรูปภาพ
    $room_picture = (isset($_POST['room_picture']) ? $_POST['room_picture'] : '');
    $upload = $_FILES['room_picture']['name'];
    if ($upload != '') {
        //ไฟล์ที่เก็บภาพ
        $path = "../image/img_room/";
        //ตัดชื่อนามสกุลภาพออกจากกัน
        $type = strrchr($_FILES['room_picture']['name'], ".");
        //ตั้งชื่อไฟล์ใหม่ สุ่มตัวเลข+วันที่
        $newname = $numrand . $date . $type;
        $path_copy = $path . $newname;
        $path_link = "../image/img_room/" . $newname;
        //คัดลอกไปยังโฟลเดอร์
        move_uploaded_file($_FILES['room_picture']['tmp_name'], $path_copy);
    } else {
        //ถ้าไม่มีการแก้ไขภาพ ใช้ภาพเดิม
        $newname = $room_picture2;
    }


    //อัพเดทข้อมูล
    $sql_2 = "UPDATE tb_room SET  room_name = '" . $_POST['room_name'] . "', room_price_adult = '" . $_POST['room_price_adult'] . "',
    room_price_child = '" . $_POST['room_price_child'] . "', room_picture1 = '" . $newname . "', room_details = '" . $_POST['room_details'] . "' WHERE room_id = '" . $id . "'";
    $query_2 = mysqli_query($Connection, $sql_2);

    header("location:room.php?update=pass");
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">

    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->

    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->

</head>

<body class="skin-blue">

    <div class="wrapper">
        <?php include '../includes/navbar_admin.php'; ?>
        <div class="content-wrapper">
            <!-- ต้องมี3บรรทัดนี้ทุกหน้า Admin -->

            <div class="container-fluid">
                <div class="row justify-content-md-center">

                    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                        <div class="row justify-content-md-center">
                            <div class="col-6"><br>

                                <div class="row justify-content-md-center">
                                    <div class="col-md-auto"><?php echo $check_submit; ?></div>
                                </div>

                                <div class="card">
                                    <h5 class="card-header">แก้ไขข้อมูลห้องพัก <?php echo ' ID : ' . $result['room_id']; ?></h5>

                                    <!-- ฟรอมในการแก้ไขข้อมูลสมาชิก -->
                                    <div class="card-body">
                                        <form method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label>ชื่อห้องพัก</label>
                                                <input type="text" class="form-control" name="room_name" value="<?php echo $result['room_name']; ?>" required />
                                            </div>
                                            <div class="form-group">
                                                <label>ราคาสำหรับผู้ใหญ่</label>
                                                <input type="number" class="form-control" name="room_price_adult" value="<?php echo $result['room_price_adult']; ?>" required />
                                            </div>
                                            <div class="form-group">
                                                <label>ราคาสำหรับเด็ก</label>
                                                <input type="number" class="form-control" name="room_price_child" value="<?php echo $result['room_price_child']; ?>" required />
                                            </div>
                                            <div class="form-group">

                                                รูปภาพเก่า <br>
                                                <img src="../image/img_room/<?php echo $result['room_picture1']; ?>" width="100px" alt="">
                                                <br>
                                                <label>รูปภาพ</label>
                                                <input type="file" name="room_picture" class="form-control" accept="image/*" onchange="readURL(this);">
                                                <img id="blah" src="#" alt="" width="250" class="img-rounded" / style="margin-top: 10px;">

                                                <!-- <input type="file" class="form-control" name="room_picture" value="<?php //echo $result['room_picture']; 
                                                                                                                        ?>" required accept="image/*" onchange="readURL(this);" /> -->
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    รายละเอียด
                                                </label>
                                                <div>
                                                    <textarea class="form-control" name="room_details" required style="height: 300px;"><?php echo $result['room_details']; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <!-- ใว้ใช้ถ้าไม่มีการเปลี่ยนภาพ ภาพเก่าจะไม่หาย -->
                                                <input type="hidden" name="room_picture2" value="<?php echo $result['room_picture']; ?>">
                                                <input type="hidden" name="room_id" value="<?php echo $room_id; ?>" />
                                                
                                                <button type="button" class="btn btn-secondary" onclick="window.location.href='room.php'">ยกเลิก</button>
                                                <button type="submit" class="btn btn-primary" name="submit">บันทึกข้อมูล</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div><br>
                    </main>

                </div>
            </div>

        </div>
    </div>

    <!-- สคริปเกี่ยวกับภาพ -->
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
    <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

    <?php mysqli_close($Connection); ?>
</body>

</html>