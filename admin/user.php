<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
  header("location:../login.php");
  exit();
} elseif ($_SESSION["user_level"] != "admin") {
  header("location:../index.php");
  exit();
}

//เช็คว่าเพิ่มสมาชิกสำเร็จมั้ย ถ้าสำเร็จ"บันทึกข้อมูลเรียบร้อยแล้ว"
if (isset($_GET["add"])) {
  if ($_GET["add"] == "pass") {
    $check_submit = check_submit_p2("บันทึกข้อมูลเรียบร้อยแล้ว");
  }
}
//เช็คว่าแก้ไขสมาชิกสำเร็จมั้ย ถ้าสำเร็จ"บันทึกข้อมูลเรียบร้อยแล้ว"
if (isset($_GET["update"])) {
  if ($_GET["update"] == "pass") {
    $check_submit = check_submit_p2("บันทึกข้อมูลเรียบร้อยแล้ว");
  }
}
//เช็คว่าลบสมาชิกสำเร็จมั้ย ถ้าสำเร็จ"บันทึกข้อมูลเรียบร้อยแล้ว"
if (isset($_GET["delete"])) {
  if ($_GET["delete"] == "pass") {
    $check_submit = check_submit_p2("ลบข้อมูลออกจากระบบเรียบร้อยแล้ว");
  }
}

//ใช้เพื่อแสดงตารางเป็นลูป
$num = 1;

$sql = "SELECT * FROM tb_user ORDER BY user_level ASC";
$query = mysqli_query($Connection, $sql);
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $title; ?></title>
  <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
  <!-- ลิงค์เรียกใช้ Data Table -->
  <link rel="stylesheet" type="text/css" href="../assets/DataTables/datatables.min.css" />

</head>

<body class="skin-blue">
  <div class="wrapper">
    <?php include '../includes/navbar_admin.php'; ?>
    <div class="content-wrapper">

      <div class="container-fluid">
        <div class="row justify-content-md-center">
          <div class="col-md-11 mt-4 mb-4">

            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 ">
              <h1 class="h2">ข้อมูลสมาชิก</h1>

              <div class="btn-toolbar mb-2 mb-md-0">
                <!-- เพิ่มข้อมูล -->
                <a href="user_add_data.php" class="btn btn-success">เพิ่มสมาชิก</a>
              </div>
            </div>
            <?php echo $check_submit; ?>

            <!-- แสดงข้อมูลเป็นตาราง -->
            <div class="card mt-3">
              <div class="card-body">
                <table class="table table-hover table-bordered mb-0" id="datatables">
                  <thead>
                    <tr class="bg-info">
                      <th scope="col" width="60px">ลำดับที่</th>
                      <th scope="col">ชื่อผู้ใช้</th>
                      <!-- <th scope="col" width="90px">รหัสผ่าน</th> -->
                      <th scope="col">ขื่อ</th>
                      <th scope="col">นามสกุล</th>
                      <th scope="col">เพศ</th>
                      <th scope="col">อีเมล์</th>
                      <th scope="col">ระดับผู้ใช้</th>
                      <th scope="col" width="60px">ตัวเลือก</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    while ($result = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
                    ?>
                      <tr>
                        <th scope="row"><?php echo $num; ?></th>
                        <td><?php echo $result["user_username"]; ?></td>
                        <!-- <td><button type="button" class="btn btn-warning btn-sm">เปลี่ยนรหัสผ่าน</button></td> -->
                        <td><?php echo $result["user_name"]; ?></td>
                        <td><?php echo $result["user_surname"]; ?></td>
                        <td><?php echo $result["user_sex"]; ?></td>
                        <td><?php echo $result["user_email"]; ?></td>
                        <td><?php if ($result["user_level"] == "member") {
                              echo "สมาชิก";
                            } else {
                              echo "ผู้ดูแลระบบ";
                            } ?></td>
                        <td>
                          <!-- ปุ่มแก้ไข -->
                          <button type="button" class="btn btn-success btn-sm" onclick="window.location.href='user_edit.php?id=<?php echo $result['user_id']; ?>'"><i class="fa fa-pencil"></i></button>
                          <!-- ลบข้อมูล-->
                          <button type="button" class="btn btn-danger btn-sm" onclick="window.location.href='user_delete.php?id=<?php echo $result['user_id']; ?>'"><i class="fa fa-trash"></i></button>
                        </td>
                      </tr>
                    <?php
                      $num++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>

  <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
  <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
  <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#datatables').DataTable();
    });
  </script>
  <script type="text/javascript" src="../assets/DataTables/datatables.min.js"></script>

  <!-- <script src="../assets/js/bootstrap.bundle.min.js"></script> -->
  <?php mysqli_close($Connection); ?>
</body>

</html>