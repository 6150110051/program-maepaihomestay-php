<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
    header("location:../login.php");
    exit();
} elseif ($_SESSION["user_level"] != "admin") {
    header("location:../index.php");
    exit();
}

//ฟังก์ชั่นวันที่
$date = date("Ymd_Hit");
//ฟังก์ชั่นสุ่มตัวเลข
$numrand = (mt_rand());

//สร้างตัวแปร
$check_submit = "";

$activity_name = "";
$activity_price_adult = "";
$activity_price_child = "";
$activity_picture = "";
$activity_time = "";
$activity_details = "";

if (isset($_POST["submit"])) {
    $strSQL = "SELECT * FROM tb_activity WHERE activity_name = '" . trim($_POST['activity_name']) . "'";
    $objQuery = mysqli_query($Connection, $strSQL);
    $objResult = mysqli_fetch_array($objQuery, MYSQLI_ASSOC);

 
    $activity_name = $_POST["activity_name"];
    $activity_price_adult = $_POST["activity_price_adult"];
    $activity_price_child = $_POST["activity_price_child"];
    // $activity_picture = $_POST["activity_picture"];
    $activity_time = $_POST["activity_time"];
    $activity_details = $_POST["activity_details"];

    //อัพโหลดรูปภาพ
    $activity_picture = (isset($_POST['activity_picture']) ? $_POST['activity_picture'] : '');
    $upload = $_FILES['activity_picture']['name'];
    if ($upload != '') {
        //ไฟล์ที่เก็บภาพ
        $path = "../image/img_activity/";
        //ตัดชื่อนามสกุลภาพออกจากกัน
        $type = strrchr($_FILES['activity_picture']['name'], ".");
        //ตั้งชื่อไฟล์ใหม่ สุ่มตัวเลข+วันที่
        $newname = $numrand . $date . $type;
        $path_copy = $path . $newname;
        $path_link = "../image/img_activity/" . $newname;
        //คัดลอกไปยังโฟลเดอร์
        //tmp_name มาจากไหน
        move_uploaded_file($_FILES['activity_picture']['tmp_name'], $path_copy);
    }

    //เช็คคีย์กิจกรรม
    if ($objResult) {
        $check_submit = '<div class="alert alert-danger" role="alert">';
        $check_submit .= '<span><i class="fa fa-exclamation"></i> คีย์กิจกรรมนี้มีอยู่แล้ว กรอกคีย์กิจกรรมใหม่</span>';
        $check_submit .= '</div>';
    } else {
        //เพิ่มเข้าดาต้าเบส เปลียนภาพ เป็น $newname
        $strSQL = "INSERT INTO tb_activity ( activity_name, activity_price_adult, activity_price_child, activity_picture1, activity_time, activity_details) 
        VALUES ('" . $_POST["activity_name"] . "','" . $_POST["activity_price_adult"] . "','" . $_POST["activity_price_child"] . "','" . $newname .  "','" . $_POST["activity_time"] . "','" . $_POST["activity_details"] . "')";
        $objQuery = mysqli_query($Connection, $strSQL);

        header("location:activity.php?add=pass");
        exit();
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
</head>

<body class="skin-blue">
    <div class="wrapper">
        <?php include '../includes/navbar_admin.php'; ?>
        <div class="content-wrapper"><br>

            <div class="container-fluid">
                <div class="row justify-content-md-center">
                    <div class="col-md-5 mb-4">

                        <div class="row justify-content-md-center">
                            <div class="col-md-auto"><?php echo $check_submit; ?></div>
                        </div>

                        <div class="card border-dark mt-2">
                            <center>
                                <h5 class="card-header">เพิ่มกิจกรรม </h5>
                            </center>
                            <div class="card-body">
                                <div class="row justify-content-md-center mb-2">
                                    <div class="col col-lg-6">
                                        <!-- <img src="images/register.png" style="width: 100%;"> -->
                                    </div>
                                </div>

                                <form method="post" enctype="multipart/form-data">
                                    
                                    <div class="form-group">
                                        <label>ชื่อกิจกรรม </label>
                                        <input type="text" class="form-control" name="activity_name" value="<?php echo $activity_name; ?>" placeholder="Enter Room Name" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label>ราคาสำหรับผู้ใหญ่</label>
                                        <input type="number" class="form-control" name="activity_price_adult" value="<?php echo $activity_price_adult; ?>" placeholder="Enter Price adult" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label>ราคาสำหรับเด็ก</label>
                                        <input type="number" class="form-control" name="activity_price_child" value="<?php echo $activity_price_child; ?>" placeholder="Enter Price child" required="" />
                                    </div>
                                    <div class="form-group">
                                        <label>รูปภาพ</label>
                                        <input type="file" class="form-control" name="activity_picture" value="<?php echo $activity_picture; ?>" placeholder="Enter Picture" required accept="image/*" />
                                    </div>
                                    <div class="form-group">
                                        <label>วันที่เข้ากิจกรรม</label>
                                        <select class="form-control" name="activity_time">
                                            <option value="รอบเช้า">รอบเช้า</option>
                                            <option value="รอบบ่าย">รอบบ่าย</option>
                                            <option value="รอบเย็น">รอบเย็น</option>
                                        </select>
                                    </div>
                                    <!-- <div class="form-group">
                                    <label>เวลาเข้ากิจกรรม</label> <br>
                                        <input type="checkbox"  name="activity_time" value="รอบเช้า">
                                        <label for="vehicle1"> รอบเช้า </label><br>
                                        <input type="checkbox"  name="activity_time" value="รอบบ่าย">
                                        <label for="vehicle2"> รอบบ่าย</label><br>
                                        <input type="checkbox"  name="activity_time" value="รอบเย็น">
                                        <label for="vehicle3"> รอบเย็น</label><br>
                                    </div> -->
                                    <div class="form-group">
                                        <label>
                                            รายละเอียด
                                        </label>
                                        <div>
                                            <textarea class="form-control" name="activity_details" required id="editor" value="<?php echo $activity_details; ?>" placeholder="Enter Room Details"></textarea>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" onclick="window.location.href='activity.php'">ยกเลิก</button>
                                        <button type="submit" class="btn btn-primary" name="submit">บันทึกข้อมูล</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
    <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

    <?php mysqli_close($Connection); ?>
</body>

</html>