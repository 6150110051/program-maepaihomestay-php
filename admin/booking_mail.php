<?php
require_once('../connections/mysqli.php');
// require_once "../assets/PHPMailer/PHPMailerAutoload.php";
// require_once "../assets/PHPMailer/PHPMailer.php";
// require_once "../assets/PHPMailer/SMTP.php";
// require_once "../assets/PHPMailer/Exception.php";

if ($_SESSION == NULL) {
    header("location:login.php");
    exit();
} elseif ($_SESSION["user_level"] != "admin") {
    header("location:../index.php");
    exit();
}

$id = $_GET["id"];

$sql = "SELECT * FROM tb_booking WHERE book_id = '" . $id . "'";
$query = mysqli_query($Connection, $sql);
$result = mysqli_fetch_array($query);


// if (isset($_POST['submit'])) {

//     $book_email = $_POST['book_email'];
//     $subject = $_POST['subject'];
//     // $book_id = $_POST['book_id'];
//     // $book_status = $_POST['book_status'];
//     // $book_desposit = $_POST['book_desposit'];
//     $detail = $_POST['detail'];
//     $sender = "From: deepicture12@gmail.com"; // ชื่อผู้ส่ง

//     if (empty($book_email) || empty($subject) || empty($detail)) {
//         $check_submit = '<div class="alert alert-danger" role="alert">';
//         $check_submit .= '<span><i class="fa fa-exclamation"></i> กรอกข้อมูลให้ครบ </span>';
//         $check_submit .= '</div>';
//     } else {
//         if (mail($book_email, $subject, $detail, $sender)) {
//             echo " Success send Mail";
//             // header("location:booking_list.php?add=pass");
//             exit();
//         } else {
//             echo " erro send Mail";
//         }
//     }
// }

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">

</head>

<body class="skin-blue">
    <div class="wrapper">
        <?php include '../includes/navbar_admin.php'; ?>
        <div class="content-wrapper">

            <div class="container-fluid">
                <div class="row justify-content-md-center">
                    <div class="col-md-5 mb-4">
                        <!-- <div class="msg"></div> -->
                        <div class="card border-dark mt-2">
                            <h5 class="card-header">ส่งเมลล์ไปยังลูกค้า รหัสการจอง : <?php echo $result["book_id"]; ?></h5>
                            <div class="card-body">
                                <div class="row justify-content-md-center mb-2">
                                    <div class="col col-lg-6">
                                        <!-- <img src="images/register.png" style="width: 100%;"> -->
                                    </div>
                                </div>
                                <form action="booking_sendEmail.php" method="post">
                                    <div class="mb-3">
                                        <label class="form-label">อีเมลล์ผู้จอง</label>
                                        <input type="email" class="form-control" name="book_email" value="<?php  echo $result["book_email"]; ?>" required />
                                        <!-- <input type="email" class="form-control" name="book_email" value="" required /> -->
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">หัวข้ออีเมลล์</label>
                                        <input type="text" class="form-control" name="subject" value="" placeholder="หัวข้ออีเมลล์" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">รายละเอียด</label>
                                        <textarea class="form-control" name="detail" value="" placeholder="รายละเอียดอีเมลล์" required></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" onclick="window.location.href='booking_list.php'">ยกเลิก</button>
                                        <!-- <button type="submit" class="btn btn-primary"  name="submit" onclick="window.location.href='booking_sendEmail.php?id=<?php //echo $result['book_id']; ?>'">ส่งอีเมลล์</button> -->
                                        <input type="submit" class="btn btn-primary" name="submit" value="ส่งอีเมลล์">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
    <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

    <?php mysqli_close($Connection); ?>
</body>

</html>