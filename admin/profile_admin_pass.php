<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
    header("location:login.php");
    exit();
} elseif ($_SESSION["user_level"] != "admin") {
    header("location:../index.php");
    exit();
}

$check_submit = "";

$sql = "SELECT user_id, user_password FROM tb_user WHERE user_username = '" . $_SESSION['user_username'] . "'";
$query = mysqli_query($Connection, $sql);
$result = mysqli_fetch_array($query);

if (isset($_POST['save'])) {
    if (md5($_POST['password_old']) != $result["user_password"]) {
        $check_submit = '<div class="alert alert-danger" role="alert">';
        $check_submit .= '<span><i class="fa fa-info-circle"></i> รหัสผ่านเดิมไม่ถูกต้อง</span>';
        $check_submit .= '</div>';
    } elseif ($_POST['password_new'] != $_POST['confirm_password']) {
        $check_submit = '<div class="alert alert-danger" role="alert">';
        $check_submit .= '<span><i class="fa fa-info-circle"></i> รหัสผ่านใหม่ ไม่ตรงกัน ยืนยันรหัสผ่านใหม่</span>';
        $check_submit .= '</div>';
    } else {
        $sql_2 = "UPDATE tb_user SET user_password = '" . md5($_POST["password_new"]) . "' WHERE user_username = '" . $_SESSION['user_username'] . "'";
        $query_2 = mysqli_query($Connection, $sql_2);

        header("location:profile_admin.php?update=pass");
        exit();
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">

</head>

<body class="skin-blue">
    <div class="wrapper">
        <?php include '../includes/navbar_admin.php'; ?>
        <div class="content-wrapper">

            <div class="container-fluid">

                <div class="row justify-content-md-center">
                    <div class="col-md-12 mt-4">
                        <center>
                            <div class="col-md-4 "><?php echo $check_submit; ?></div>
                        </center>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-md-5 mb-4">
                        <div class="card border-dark mt-2">
                            <h5 class="card-header">เปลี่ยนรหัสผ่าน ID : <?php echo $result["user_id"]; ?></h5>
                            <div class="card-body">
                                <div class="row justify-content-md-center mb-2">
                                    <div class="col col-lg-6">
                                        <!-- <img src="images/password.png" style="width: 100%;"> -->
                                    </div>
                                </div>
                                <form method="post">
                                    <div class="mb-3">
                                        <label class="form-label">รหัสผ่านเดิม</label>
                                        <input type="password" class="form-control" name="password_old" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">รหัสผ่านใหม่</label>
                                        <input type="password" class="form-control" name="password_new" required />
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label">ยืนยันรหัสผ่านใหม่</label>
                                        <input type="password" class="form-control" name="confirm_password" required />
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" onclick="window.location.href='profile_admin.php'">ยกเลิก</button>
                                        <button type="submit" class="btn btn-success" name="save">บันทึกข้อมูล</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
    <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

    <?php mysqli_close($Connection); ?>
</body>

</html>