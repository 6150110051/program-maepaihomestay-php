<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
  header("location:../login.php");
  exit();
} elseif ($_SESSION["user_level"] != "admin") {
  header("location:../index.php");
  exit();
}

//รับค่าไอดีมา
$id = $_GET["id"];

//แสดงข้อมูลเฉพาะไอดีที่รับมา
$sql = "SELECT * FROM tb_user WHERE user_id = '" . $id . "'";
$query = mysqli_query($Connection, $sql);
$result = mysqli_fetch_array($query, MYSQLI_ASSOC);

//ตรวจสอบการแก้ไข
if (isset($_POST["submit"])) {
  $sql_2 = "UPDATE tb_user SET user_username = '" . $_POST['user_username'] . "', user_name = '" . $_POST['user_name'] . "', user_surname = '" . $_POST['user_surname'] . "',
    user_sex = '" . $_POST['user_sex'] . "', user_email = '" . $_POST['user_email'] . "', user_level = '" . $_POST['user_level'] . "' WHERE user_id = '" . $id . "'";
  $query_2 = mysqli_query($Connection, $sql_2);

  header("location:user.php?update=pass");
  exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $title; ?></title>
  <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
  <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">

</head>

<body class="skin-blue">

  <div class="wrapper">
    <?php include '../includes/navbar_admin.php'; ?>
    <div class="content-wrapper">
      <!-- ต้องมี3บรรทัดนี้ทุกหน้า Admin -->

      <div class="container-fluid">
        <div class="row justify-content-md-center">

          <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <div class="row justify-content-md-center">
              <div class="col-6"><br>
                <div class="card">
                  <h5 class="card-header">แก้ไขข้อมูลสมาชิก <?php echo ' ID : ' . $result['user_id']; ?></h5>

                  <!-- ฟรอมในการแก้ไขข้อมูลสมาชิก -->
                  <div class="card-body">
                    <form method="post">
                      <div class="mb-3">
                        <label class="form-label">ชื่อผู้ใช้</label>
                        <input type="text" class="form-control" name="user_username" value="<?php echo $result['user_username']; ?>" required />
                      </div>
                      <div class="mb-3">
                        <label class="form-label">ชื่อ</label>
                        <input type="text" class="form-control" name="user_name" value="<?php echo $result["user_name"]; ?>" required />
                      </div>
                      <div class="mb-3">
                        <label class="form-label">นามสกุล</label>
                        <input type="text" class="form-control" name="user_surname" value="<?php echo $result["user_surname"]; ?>" required />
                      </div>
                      <div class="mb-3">
                        <label class="form-label">เพศ</label>
                        <select class="form-select" name="user_sex">
                          <option value="ชาย" <?php if ($result["user_sex"] == 'ชาย') {
                                                echo " selected";
                                              } ?>>ชาย</option>
                          <option value="หญิง" <?php if ($result["user_sex"] == 'หญิง') {
                                                  echo " selected";
                                                } ?>>หญิง</option>
                        </select>
                      </div>
                      <div class="mb-3">
                        <label class="form-label">วันเกิด</label>
                        <input type="date" class="form-control" name="user_birthdate" value="<?php echo $result["user_birthdate"]; ?>" required />
                      </div>
                      <div class="mb-3">
                        <label class="form-label">เบอร์</label>
                        <input type="text" class="form-control" name="user_phone" value="<?php echo $result["user_phone"]; ?>" required />
                      </div>
                      <div class="mb-3">
                        <label class="form-label">ไลน์</label>
                        <input type="text" class="form-control" name="user_line" value="<?php echo $result["user_line"]; ?>" required />
                      </div>
                      <div class="mb-3">
                        <label class="form-label">เฟสบุ๊ค</label>
                        <input type="text" class="form-control" name="user_facebook" value="<?php echo $result["user_facebook"]; ?>" required />
                      </div>
                      <div class="mb-3">
                        <label class="form-label">อีเมล์</label>
                        <input type="email" class="form-control" name="user_email" value="<?php echo $result["user_email"]; ?>" required />
                      </div>

                      <div class="mb-3">
                        <label class="form-label">ระดับผู้ใช้</label>
                        <select class="form-select" name="user_level">
                          <option value="member" <?php if ($result["user_level"] == 'member') {
                                                    echo " selected";
                                                  } ?>>สมาชิก</option>
                          <option value="admin" <?php if ($result["user_level"] == 'admin') {
                                                  echo " selected";
                                                } ?>>ผู้ดูแลระบบ</option>
                        </select>
                      </div>

                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" onclick="window.location.href='user.php'">ยกเลิก</button>
                        <button type="submit" name="submit" class="btn btn-primary">บันทึก</button>
                      </div>
                    </form>
                  </div>

                </div>
              </div>
            </div><br>
          </main>

        </div>
      </div>

    </div>
  </div>



  <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
  <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
  <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

  <?php mysqli_close($Connection); ?>
</body>

</html>