<?php

header('Content-Type: application/json');

require_once('../connections/mysqli.php');

$sql = "SELECT * FROM bookingroomdetail ORDER BY book_drt_id ASC";
$result = mysqli_query($Connection, $sql);

$data = array();
$num_book = array();
$num_activity = array();

foreach ($result as $row ) {
  $data[] = $row;
}
mysqli_close($Connection);

echo json_encode($data);

?>