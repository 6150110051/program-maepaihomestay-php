<?php
require_once('../connections/mysqli.php');

// ตรวจสอบว่ามีการเข้าสู่ระบบหรือยัง 
if ($_SESSION == NULL) {
  header("location:../login.php");
  exit();
  //ไว้ตรวจสอบป้องกัน member เข้าระบบแอดมิน
} elseif ($_SESSION["user_level"] != "admin") {
  header("location:../index.php");
  exit();
}

//รายการจอง
$num_book = 0;
$sql = "SELECT * FROM tb_booking ORDER BY book_id ASC";
$query = mysqli_query($Connection, $sql);
while ($result = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
  $num_book++;
}

//ห้องพัก
$num_room = 0;
$sql2 = "SELECT * FROM tb_room ORDER BY room_id ASC";
$query2 = mysqli_query($Connection, $sql2);
while ($result2 = mysqli_fetch_array($query2, MYSQLI_ASSOC)) {
  $num_room++;
}

//กิจกรรม
$num_activity = 0;
$sql3 = "SELECT * FROM tb_activity ORDER BY activity_id ASC";
$query3 = mysqli_query($Connection, $sql3);
while ($result3 = mysqli_fetch_array($query3, MYSQLI_ASSOC)) {
  $num_activity++;
}

//สมาชิก
$num_member = 0;
$sql4 = "SELECT * FROM tb_user ORDER BY user_id ASC";
$query4 = mysqli_query($Connection, $sql4);
while ($result4 = mysqli_fetch_array($query4, MYSQLI_ASSOC)) {
  $num_member++;
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $title; ?></title>
  <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
  <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
  <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">


</head>

<body class="skin-blue">

  <div class="wrapper">
    <?php include '../includes/navbar_admin.php'; ?>
    <div class="content-wrapper">
      <!-- ต้องมี3บรรทัดนี้ทุกหน้า Admin -->

      <div class="container">
        <div class="container-fluid">
          <!-- Content Header (Page header) -->
          <section class="content-header">

            <h1>
              Dashboard
              <small>แม่ไพโฮมสเตย์</small>
            </h1>

          </section>

          <!-- Main content -->
          <section class="content container-fluid">
            <!--| Your Page Content Here | -->

            <div class="row">
              <div class="col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>
                  <div class="info-box-content">
                    <h5>รายการจอง</h5>
                    <span class="info-box-number"><?php echo $num_book; ?> <small>รายการ</small></span>
                  </div><!-- /.info-box-content -->
                </div>
              </div>
              <div class="col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-aqua"><i class="fa fa-bed" aria-hidden="true"></i></i></span>
                  <div class="info-box-content">
                    <h5>ห้องพัก</h5>

                    <span class="info-box-number"><?php echo $num_room; ?> <small>ห้อง</small></span>
                  </div><!-- /.info-box-content -->
                </div>
              </div>
              <div class="col-md-3">
                <div class="info-box">
                  <!-- Apply any bg-* class to to the icon to color it -->
                  <span class="info-box-icon bg-red"><i class="fa fa-star-o"></i></span>
                  <div class="info-box-content">
                    <h5>กิจกรรม</h5>
                    <span class="info-box-number"><?php echo $num_activity; ?> <small>กิจกรรม</small></span>
                  </div><!-- /.info-box-content -->
                </div>
              </div>
              <div class="col-md-3">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-users" aria-hidden="true"></i></span>
                  <div class="info-box-content">
                    <h5>สมาชิก<h5>
                        <span class="info-box-number"><?php echo $num_member - 1; ?> <small>คน</small></span>
                  </div><!-- /.info-box-content -->
                </div>
              </div>

              <!-- แผนภูมิ -->
              <div class="col-md-6">
                <canvas id="myChart" width="400" height="400"></canvas>
              </div>
              <div class="col-md-6">
                <canvas id="myChart2" width="400" height="400"></canvas>
              </div>

            </div>
          </section>
        </div>
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  </div>

  <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
  <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
  <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/chart.js@3.5.0/dist/chart.min.js"></script>

  <script>
    $(document).ready(function() {
      showGrapt();
    });

    function showGrapt() {
      $.post('data_chart.php', function(data) {
        console.log(data);
        let book_drt_id = [];
        let r_price = [];

        for (let i in data) {
          book_drt_id.push(data[i].book_drt_id);
          r_price.push(data[i].r_price);
        }

        let chatdata = {
          labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
          datasets: [{
            label: '# of Votes',
            data: r_price,
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)'
            ],
          }]
        };

        let graphTarget = $('#myChart');
        let myChart = new Chart(graphTarget,{
        type: 'bar',
        data: chatdata
      })



      })
    }
  </script>


<script>
    $(document).ready(function() {
      showGrapt();
    });

    function showGrapt() {
      $.post('data_chart.php', function(data) {
        console.log(data);
        let book_drt_id = [];
        let r_price = [];

        for (let i in data) {
          book_drt_id.push(data[i].book_drt_id);
          r_price.push(data[i].r_price);
        }

        let chatdata = {
          labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
          datasets: [{
            label: '# of Votes',
            data: r_price,
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)'
            ],
          }]
        };

        let graphTarget = $('#myChart2');
        let myChart = new Chart(graphTarget,{
        type: 'doughnut',
        data: chatdata
      })



      })
    }
  </script>

  <!-- <script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  </script> -->

  <!-- <script>
    var ctx = document.getElementById('myChart2').getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      }
    });
  </script> -->


  <?php mysqli_close($Connection); ?>
</body>

</html>