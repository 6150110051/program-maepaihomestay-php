<?php
require_once('../connections/mysqli.php');

if ($_SESSION == NULL) {
    header("location:../login.php");
    exit();
} elseif ($_SESSION["user_level"] != "admin") {
    header("location:../index.php");
    exit();
}

//รับค่าไอดีมา
$id = $_GET["id"];

//แสดงข้อมูลเฉพาะไอดีที่รับมา
$sql = "SELECT * FROM tb_booking WHERE book_id = '" . $id . "'";
$query = mysqli_query($Connection, $sql);
$result = mysqli_fetch_array($query, MYSQLI_ASSOC);


if (isset($_POST["submit"])) {

    //อัพเดทข้อมูล
    $sql_2 = "UPDATE tb_booking SET 
    book_name = '" . $_POST['book_name'] . "',
    book_phone = '" . $_POST['book_phone'] . "',
    book_email = '" . $_POST['book_email'] . "',
    book_status = '" . $_POST['book_status'] . "' 

    WHERE book_id = '" . $id . "'";
    $query_2 = mysqli_query($Connection, $sql_2);

    header("location:booking_list.php?update=pass");
    exit();
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/AdminLTE.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/skin-blue.min.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="../assets/font-awesome-4.7.0/css/font-awesome.min.css">

    <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> -->

    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> -->

</head>

<body class="skin-blue">

    <div class="wrapper">
        <?php include '../includes/navbar_admin.php'; ?>
        <div class="content-wrapper">
            <!-- ต้องมี3บรรทัดนี้ทุกหน้า Admin -->

            <div class="container-fluid">
                <div class="row justify-content-md-center">

                    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                        <div class="row justify-content-md-center">
                            <div class="col-6"><br>

                                <div class="row justify-content-md-center">
                                    <div class="col-md-auto"><?php echo $check_submit; ?></div>
                                </div>

                                <div class="card">
                                    <h5 class="card-header">อัพเดทข้อมูลจองห้องพัก <?php echo ' ID : ' . $result['book_id']; ?></h5>

                                    <!-- ฟรอมในการแก้ไขข้อมูลสมาชิก -->
                                    <div class="card-body">
                                        <form method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <label>วันที่จอง</label>
                                                <input type="text" class="form-control" name="book_date" value="<?php echo $result['book_date']; ?>" disabled />
                                            </div>
                                            <div class="form-group">
                                                <label>ชื่อผู้จอง</label>
                                                <input type="text" class="form-control" name="book_name" value="<?php echo $result['book_name']; ?>" required />
                                            </div>
                                            <div class="form-group">
                                                <label>เบอร์โทร</label>
                                                <input type="number" class="form-control" name="book_phone" value="<?php echo $result['book_phone']; ?>" required />
                                            </div>
                                            <div class="form-group">
                                                <label>อีเมลล์</label>
                                                <input type="email" class="form-control" name="book_email" value="<?php echo $result['book_email']; ?>" required />
                                            </div>
                                            <div class="form-group">
                                                รูปภาพเก่า <br>
                                                <img src="../imgslip/<?php echo $result['book_slip'];?>" width="350px" alt="">
                                                <br>
                                                
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">สถานะการจอง</label>
                                                <select class="form-select" name="book_status">
                                                    <option value="1" <?php if ($result["book_status"] == 1) {
                                                                            echo " selected";
                                                                        } ?>>รออนุมัติ</option>
                                                    <option value="2" <?php if ($result["book_status"] == 2) {
                                                                                echo " selected";
                                                                            } ?>>อนุมัติ(รอชำระเงิน)</option>
                                                    <option value="3" <?php if ($result["book_status"] == 3) {
                                                                            echo " selected";
                                                                        } ?>>จองสำเร็จ</option>
                                                    <option value="4" <?php if ($result["book_status"] == 4) {
                                                                                echo " selected";
                                                                            } ?>>ยกเลิก</option>
                                                </select>
                                            </div>

                                            <div class="modal-footer">

                                                <button type="button" class="btn btn-secondary" onclick="window.location.href='booking_list.php'">ยกเลิก</button>
                                                <button type="submit" class="btn btn-primary" name="submit">บันทึกข้อมูล</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div><br>
                    </main>

                </div>
            </div>

        </div>
    </div>

    <!-- สคริปเกี่ยวกับภาพ -->
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script type="text/javascript" src="../assets/jquery/jquery-slim.min.js"></script>
    <script type="text/javascript" src="../assets/popper/popper.min.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/adminlte.min.js"></script>

    <?php mysqli_close($Connection); ?>
</body>

</html>