<?php
include('header/h_nav.php');
// เช็คการเข้า
if ($_SESSION['user_username'] == '') {
    header("Location: index.php");
}
$user_id = $_SESSION['user_id']; //ไอดีสมาชิกที่มีการเรียกจากการใช้ session
// echo $user_id ;
// exit;

// echo '<pre>';
// print_r($_SESSION);
// echo '<pre>';
$qmember = "SELECT user_username , user_email , user_phone , user_surname, user_name FROM tb_user WHERE user_id = $user_id";
$rsmember = mysqli_query($con, $qmember) or die("Error in query: $qmember " . mysqli_error($con));
$rowmember = mysqli_fetch_array($rsmember);

// print_r($rowmember);
// exit;



?>
<title>Document</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">


<style>
    .main-content {
        padding-top: 100px;
        padding-bottom: 100px;
    }

    a {
        text-decoration: none;
    }

    .food-card {
        background: #fff;
        border-radius: 5px;
        overflow: hidden;
        margin-bottom: 30px;
        -webkit-box-shadow: 0 2px 10px rgba(0, 0, 0, 0.06);
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.06);
        -webkit-transition: 0.1s;
        transition: 0.1s;
    }

    .food-card:hover {
        -webkit-box-shadow: 0 5px 20px rgba(0, 0, 0, 0.1);
        box-shadow: 0 5px 20px rgba(0, 0, 0, 0.1);
    }

    .food-card .food-card_img {
        display: block;
        position: relative;
    }

    .food-card .food-card_img img {
        width: 100%;
        height: 200px;
        -o-object-fit: cover;
        object-fit: cover;
    }

    .food-card .food-card_img i {
        position: absolute;
        top: 20px;
        right: 30px;
        color: #fff;
        font-size: 25px;
        -webkit-transition: all 0.1s;
        transition: all 0.1s;
    }

    .food-card .food-card_img i:hover {
        top: 18px;
        right: 28px;
        font-size: 29px;
    }

    .food-card .food-card_content {
        padding: 15px;
    }

    .food-card .food-card_content .food-card_title-section {

        overflow: hidden;
    }

    .food-card .food-card_content .food-card_title-section .food-card_title {
        font-size: 24px;
        color: #333;
        font-weight: 500;
        display: block;
        line-height: 1.3;
        margin-bottom: 8px;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .food-card .food-card_content .food-card_title-section .food-card_author {
        font-size: 15px;
        display: block;
    }

    .food-card .food-card_content .food-card_bottom-section .space-between {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
    }

    .food-card .food-card_content .food-card_bottom-section .food-card_subscribers {
        margin-left: 17px;
    }

    .food-card .food-card_content .food-card_bottom-section .food-card_subscribers img,
    .food-card .food-card_content .food-card_bottom-section .food-card_subscribers .food-card_subscribers-count {
        height: 45px;
        width: 45px;
        border-radius: 45px;
        border: 3px solid #fff;
        margin-left: -17px;
        float: left;
        background: #f5f5f5;
    }

    .food-card .food-card_content .food-card_bottom-section .food-card_subscribers .food-card_subscribers-count {
        position: relative;
    }

    .food-card .food-card_content .food-card_bottom-section .food-card_subscribers .food-card_subscribers-count span {
        position: absolute;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
        font-size: 13px;
    }

    .food-card .food-card_content .food-card_bottom-section .food-card_price {
        font-size: 25px;
        font-weight: 500;
        color: #F47A00;
    }



    .food-card .food-card_content .food-card_bottom-section input {
        background: #f5f5f5;
        border-color: #f5f5f5;
        -webkit-box-shadow: none;
        box-shadow: none;
        text-align: center;
    }

    .food-card .food-card_content .food-card_bottom-section {
        border-color: #f5f5f5;
        border-width: 3px;
        -webkit-box-shadow: none;

        box-shadow: none;
    }

    @media (min-width: 992px) {
        .food-card--vertical {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            position: relative;
            height: 235px;
        }

        .food-card--vertical .food-card_img img {
            width: 300px;
            height: 100%;
            -o-object-fit: cover;
            object-fit: cover;
        }
    }

    .cart_content {
        padding-top: 6%;
    }
</style>
</head>

<body>


    <div class="container cart_content">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12">
                <form method="post" action="cart_saveorder.php">
                    <div class="row">
                        <div class="food-card">
                            <div class="food-card_content">
                                <div class="food-card_bottom-section">
                                    <div class="food-card_bottom-section food-card_price">
                                        <h1>ข้อมูลการจอง</h1>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <div class="row" style=" padding-bottom:1%;">
                            <div class='col-md-2'><a class="btn btn-success btn-sm" href="index.php">กลับไปหน้าแรก</a></div>
                            <div class='col-md-2'> <a class="btn btn-success btn-sm" id="btnEmpty" href="cart.php?act=empty">ยกเลิกการสั่งจอง</a></div>
                        </div>
                        <?php
                        // cart_room
                        if (isset($_SESSION["cart_room"])) {
                            foreach ($_SESSION["cart_room"] as $room) {

                                //คำนวณห้องพัก
                                $room_price = ($room["room_price_adult"] * $room["n_adult"]) + ($room["room_price_child"] * $room["n_child"]); // ผลรวมในราคาแต่ละห้องพัก
                                $total_room_price += $room_price;  // ผลรวมทั้งหมดของห้องพัก
                                $desposit_room = ($total_room_price * 20 / 100); // มัดจำที่ต้องจ่าย 20 % ของหองพัก
                                // ปิด คำนวณห้องพัก

                        ?>
                                <div class="col-md-9 food-card">
                                    <div class="row food-card_content">
                                        <div class='col-md-2 '><img src="image/img_room/<?php echo $room["room_picture1"]; ?>" width="100%" class="cart-item-image" /></div>
                                        <div class='col-md-5'>
                                            <div class="row"><?php echo " ชื่อรายการจอง " . " : " . $room["room_name"]; ?></div>
                                            <div class="row"><?php echo " รหัสห้อง " . " : " . $room["room_id"]; ?> </div>
                                            <div class="row"><?php echo " วันที่เข้าพัก " . " : " .   $room["date_start"]; ?> </div>
                                            <div class="row"><?php echo " วันที่ออก " . " : " . $room["date_end"]; ?> </div>
                                        </div>
                                        <div class='col-md-3'></div>
                                        <div class='col-md-2 food-card_content'>
                                            <div class="row">
                                                <label for="inputCity" class="form-label">ผู้ใหญ่</label>
                                                <input type="text" class="form-control form-control-sm" name="" value="<?php echo $room["n_adult"]; ?>" readonly>
                                            </div>
                                            <div class="row">
                                                <label for="inputCity" class="form-label">เด็ก(4-11)</label>
                                                <input type="text" class="form-control form-control-sm" name="" value="<?php echo $room["n_child"]; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <hr>
                                            
                                            <div class='col-md-12' align="right"> <?php echo "฿" . number_format($room_price, 2); ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        // closs cart_room

                        // cart_activity
                        if (isset($_SESSION["cart_activity"])) {
                            foreach ($_SESSION["cart_activity"] as $activity) {
                                //คำนวณกิจกรรม
                                $activity_price = ($activity["activity_price_adult"] * $activity["n_adult"]) + ($activity["activity_price_child"] * $activity["n_child"]); // ผลรวมในราคาแต่ละห้องพัก
                                $total_activity_price += $activity_price;  // ผลรวมทั้งหมดของกิจกรรม
                                $desposit_activity = ($total_activity_price * 20 / 100); // มัดจำที่ต้องจ่าย 20 % ของกิจกรรม
                                // ปิด คำนวณกิจกรรม
                            ?> 
                                <div class="col-md-9 food-card">
                                    <div class="row food-card_content">
                                        <div class='col-md-2 '><img src="image/img_activity/<?php echo $activity["activity_picture1"]; ?>" width="100%" class="cart-item-image" /></div>
                                        <div class='col-md-5'>
                                            <div class="row"><?php echo " ชื่อรายการจอง " . " : " . $activity["activity_name"]; ?></div>
                                            <div class="row"><?php echo " รหัสห้อง " . " : " . $activity["activity_id"]; ?> </div>
                                            <div class="row"><?php echo " วันที่เข้าร่วมกิจกรรม " . " : " .   $activity["activity_date"]; ?> </div>
                                            <div class="row"><?php echo " ช่วงเวลาของการเข้ากิจกรรม " . " : " . $activity["activity_time"]; ?> </div>
                                        </div>
                                        <div class='col-md-3'></div>
                                        <div class='col-md-2 food-card_content'>
                                            <div class="row">
                                                <label for="inputCity" class="form-label">ผู้ใหญ่</label>
                                                <input type="text" class="form-control form-control-sm" name="" value="<?php echo $activity["n_adult"]; ?>" readonly>
                                            </div>
                                            <div class="row">
                                                <label for="inputCity" class="form-label">เด็ก(4-11)</label>
                                                <input type="text" class="form-control form-control-sm" name="" value="<?php echo $activity["n_child"]; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <hr>
                                            <div class='col-md-12' align="right"> <?php echo "฿" . number_format($activity_price, 2); ?></div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        if (empty($_SESSION["cart_activity"]) && empty($_SESSION["cart_room"])) {
                            ?>
                            <h1 align="center">- ไม่รายการสินค้าของคุณอยู่ กรุณากลับไปเลือกสินค้าใหม่ -</h1>
                        <?php  } ?>

                        <?php
                        // คำนวณรายการทั้งหมด
                        $total_price = $total_room_price +  $total_activity_price;
                        $desposit =  $desposit_room +  $desposit_activity;
                        ?>
                        <div class="col-md-3 ">
                            <div class="food-card">
                                <div class="food-card_content">
                                    <div class="food-card_title-section">
                                        <span>
                                            <p>ยอดรวมทั้งสิ้น</p>
                                        </span>
                                        <p><strong><?php echo "฿" . number_format($total_price, 2); ?></strong></p>
                                    </div>
                                    <hr>
                                    <div class="food-card_bottom-section">
                                        <div class="space-between">
                                            <strong>ต้องชำระมัดจำ</strong>
                                            <div class="food-card_price">
                                                <p><strong><?php echo "฿" . number_format($desposit, 2) ?></strong></p>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="desposit" value="<?php echo number_format($desposit, 2); ?>">
                                    <input type="hidden" name="total_price" value="<?php echo number_format($total_price, 2); ?>">
                                    <!-- <input class="btn btn-success " type="submit" name="Submit2" value="ชำระค่ามัดจำ" /> -->
                                </div>
                            </div>
                        </div>
                        <div class="food-card">
                            <div class="food-card_content">
                                <div class="food-card_bottom-section">
                                    <div class="food-card_bottom-section food-card_price">
                                        <h1>รายละเอียดในการติดต่อ</h1>
                                    </div>
                                    <hr>
                                </div>
                                <div class="mb-3">
                                    <label for="exampleInputEmail1" class="form-label">ชื่อ - นามสกุล ผู้จอง</label>
                                    <input type="text" class="form-control" name="user_username" id="user_username" aria-describedby="emailHelp" value="<?php echo $rowmember['user_name'] . ' ' . $rowmember['user_surname']; ?>" required>
                                    <div id="emailHelp" class="form-text">หากผู้จองเป็นผู้ใช่ user นี้อยู่แล้วไม่ต้องกรอก</div>
                                </div>
                                <div class="row  g-3">

                                    <div class="col-md-6">
                                        <label for="inputEmail4" class="form-label">Email</label>
                                        <input type="user_email" name="user_email" class="form-control" id="inputEmail4" value="<?php echo $rowmember['user_email']; ?>" required>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="exampleInputPassword1" class="form-label">เบอร์ติดต่อ</label>
                                        <input class="form-control" name="user_phone" type="text" id="user_phone" value="<?php echo $rowmember['user_phone']; ?>" required>
                                    </div>
                                    <div class="col-12">

                                        <input type="hidden" name="desposit" value="<?php echo $desposit; ?>">
                                        <input type="hidden" name="total_price" value="<?php echo $total_price; ?>">
                                        <button type="submit" class="btn btn-primary">ยืนยันการจอง</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                </form>
            </div>
        </div>
    </div>
</body>

</html>