-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2021 at 10:18 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maepaihomestay`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookingactivitydetail`
--

CREATE TABLE `bookingactivitydetail` (
  `activity_dt_id` int(10) NOT NULL,
  `book_id` int(11) NOT NULL,
  `activity_id` int(8) NOT NULL,
  `activity_date` date NOT NULL,
  `activity_time` enum('เช้า','บ่าย','เย็น') COLLATE utf8_unicode_ci NOT NULL,
  `a_num_adult` int(10) NOT NULL,
  `a_num_child` int(10) NOT NULL,
  `a_price` double(10,2) NOT NULL COMMENT 'ราคาต่อ 1 กิจกรรม',
  `a_detail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookingactivitydetail`
--

INSERT INTO `bookingactivitydetail` (`activity_dt_id`, `book_id`, `activity_id`, `activity_date`, `activity_time`, `a_num_adult`, `a_num_child`, `a_price`, `a_detail`) VALUES
(123, 124, 1, '2021-10-20', 'เช้า', 5, 7, 3650.00, NULL),
(122, 124, 2, '2021-10-08', 'เย็น', 3, 0, 1500.00, NULL),
(121, 123, 3, '2021-09-29', 'บ่าย', 3, 0, 1800.00, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bookingroomdetail`
--

CREATE TABLE `bookingroomdetail` (
  `room_dt_id` int(10) NOT NULL,
  `book_id` int(10) NOT NULL,
  `room_id` int(10) NOT NULL,
  `r_check_in` date NOT NULL,
  `r_check__out` date NOT NULL,
  `r_num_adult` int(10) NOT NULL,
  `r_num_child` int(10) NOT NULL,
  `r_price` double(10,2) NOT NULL COMMENT 'ราคาต่อรายการ',
  `r_detail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bookingroomdetail`
--

INSERT INTO `bookingroomdetail` (`room_dt_id`, `book_id`, `room_id`, `r_check_in`, `r_check__out`, `r_num_adult`, `r_num_child`, `r_price`, `r_detail`) VALUES
(111, 121, 3, '2021-10-01', '2021-10-02', 3, 3, 2700.00, NULL),
(113, 122, 1, '2021-09-29', '2021-10-08', 3, 0, 1350.00, NULL),
(112, 122, 2, '2021-10-01', '2021-10-07', 6, 8, 5000.00, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_activity`
--

CREATE TABLE `tb_activity` (
  `activity_id` int(10) NOT NULL COMMENT 'รหัสกิจกรรม',
  `activity_name` varchar(30) NOT NULL COMMENT 'ชื่อกิจกรรม',
  `activity_price_adult` double(5,2) NOT NULL COMMENT 'ราคาผู้ใหญ่',
  `activity_price_child` double(5,2) NOT NULL COMMENT 'ราคาเด็ก',
  `activity_picture1` text NOT NULL COMMENT 'รูป',
  `activity_picture2` text NOT NULL COMMENT 'รูปภาพกิจกรรม2',
  `activity_picture3` text NOT NULL COMMENT 'รูปภาพกิจกรรม3',
  `activity_time` enum('รอบเช้า','รอบบ่าย','รอบเย็น') DEFAULT 'รอบเช้า' COMMENT 'เวลาเข้ารับบริการ',
  `activity_details` varchar(500) NOT NULL COMMENT 'รายละเอียดกิจกรรม',
  `activity_datesave` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'วันเวลาที่อัพเดทข้อมูลกิจกรรม'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_activity`
--

INSERT INTO `tb_activity` (`activity_id`, `activity_name`, `activity_price_adult`, `activity_price_child`, `activity_picture1`, `activity_picture2`, `activity_picture3`, `activity_time`, `activity_details`, `activity_datesave`) VALUES
(1, 'ล่องแพกอนโดล่า', 450.00, 200.00, 'acty1.jpg', 'image/img_activity/acty1.jpg', 'image/img_activity/acty1.jpg', 'รอบเช้า', 'ทริปล่องแพ ที่ใครๆก็มาพักผ่อนกันได้ พร้อมโฮมสเตย์สำหรับบ้านสวน\r\n เป็นอีกหนึ่งสถานที่ของ จ.พังงา ที่ไม่ควรพลาด ชมธรรมชาติในระหว่าง\r\nทางของการล่องแพไม่ว่าจะเป็นต้นไม้นานาชนิด หรือแม่แต่สัตว์เล็กสัตว์น้อย\r\nก็จะอยู่ลิมคลองทั้งสองข้างทาง\r\nแพลำนึงสามารถนั่งได้ 2 -10 คน ถ้าไม่อยากเหงา แนนำเลย\r\nต้องมากันเป็นครอบครัวจะสนุกสุดๆ\r\n\r\nเดินทางจากตลาดท้ายเหมือง เพียง 10 กิโลเมตร เท่านั้น', '2021-09-01 03:43:54'),
(2, 'เดินป่าเขาหินลับ', 500.00, 250.00, 'acty3-3.jpg', 'image/img_activity/acty3-3.jpg', 'image/img_activity/acty3-3.jpg', 'รอบเช้า', 'ทริปนี้พาเดินป่า ตั้งแคมป์ชิลๆ ริมคลองราชาซึ่งเป็นที่ที่ห่างจากเมือ\r\nงอยู่พอสมควร \r\n\r\nใช้เวลาในการเดินป่า เพียง 4 ชม. มีสถานที่ที่ดีต่อใจ\r\nเป็นธรรมชาติโดยแบบสมบูรณ์\r\nระยะทางราวประมาณ 4 กิโล มีต้นไม้เก่าแก่มากมาย \r\nระหว่างทางจะมีน้ำตกสูง ราวๆ 10 เมตร ที่ขึ้นไปแล้วจะเป็นดั้งสายรุ่งยามเย็นตลอดเวลา\r\nในการเดินทางทุกครั้งจพต้องมีการเตรียมพร้อมหลายประการ \r\nให้ลูกค้ามาก่อนเวลา ประมาณ 30 น. ก่อนถึงเวลานัดหมาย', '2021-09-01 03:43:54'),
(3, 'ล่องแก่งแคนนู', 600.00, 300.00, '111310988120211005_133431.jpg', 'image/img_activity/acty2-3.jpg', 'image/img_activity/acty2-3.jpg', 'รอบเช้า', 'ถ้าอยากไปเที่ยวสวน แต่เบื่อกับการนั่งเล่น เบื่อกับการเสียตังแพงๆเพื่อไปดูอาปาก้าแล้วละก็.... \r\nลองไปล่องแก่ง ที่แก่งที่แม่ไพโฮมส์สเตย์ \r\n- เส้นทางการมา เพื่อนๆ สามารถเปิด GPS และเซิสว่าแม่ไพโฮมสเตย์ ล่องแพกอนโดล่า ได้เลย\r\n- ส่วนระยะล่องแก่งประมาณ 5-6 กิโลเมตร \r\n- ส่วนเรื่องถ่ายรูป สามารถให้เจ้าหน้าที่ถ่ายรูปได้ ความจริงมีค่าใช้จ่าย (ประมาณ 200 บาท)\r\n- ความปลอดภัยหายห่วง มีหมวกกันน๊อค และสตาฟท์นำทาง 1 คน และสตาฟท์ดูแลพวกเราอีก 2 คน \r\n- นอกจากล่องแก่งสามารถเที่ยว สวนแม่ไพได้อีกด้วย\r\n', '2021-09-01 03:43:54');

-- --------------------------------------------------------

--
-- Table structure for table `tb_booking`
--

CREATE TABLE `tb_booking` (
  `book_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL COMMENT 'ไอดีของสมาชิก',
  `book_date` datetime NOT NULL,
  `book_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `book_phone` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `book_email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `book_desposit` float NOT NULL,
  `book_total` float NOT NULL,
  `book_status` int(1) NOT NULL COMMENT '1 = รออนุมัติ \r\n2 = อนุมัติ(รอชำระเงิน) 3 = จองสำเร็จ \r\n4 = ยกเลิก	',
  `book_slip` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `book_slip_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_booking`
--

INSERT INTO `tb_booking` (`book_id`, `user_id`, `book_date`, `book_name`, `book_phone`, `book_email`, `book_desposit`, `book_total`, `book_status`, `book_slip`, `book_slip_date`) VALUES
(124, 2, '2021-10-05 15:15:41', 'แพทริค ฟิงค์เลอร์', '0991234567', 'patrick@gmail.com', 1030, 5150, 4, NULL, '0000-00-00'),
(123, 2, '2021-10-05 15:15:14', 'แพทริค ฟิงค์เลอร์', '0991234567', 'patrick@gmail.com', 360, 1800, 1, NULL, '0000-00-00'),
(122, 2, '2021-10-05 15:14:57', 'แพทริค ฟิงค์เลอร์', '0991234567', 'patrick@gmail.com', 1270, 6350, 3, 'slip48681694420211005_151640.jpg', '2021-10-05'),
(121, 2, '2021-10-05 15:13:56', 'แพทริค ฟิงค์เลอร์', '0991234567', 'patrick@gmail.com', 540, 2700, 2, NULL, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_room`
--

CREATE TABLE `tb_room` (
  `room_id` int(10) NOT NULL COMMENT 'รหัสห้อง',
  `room_name` varchar(30) NOT NULL COMMENT 'ชื่อห้อง',
  `room_price_adult` double(5,2) NOT NULL COMMENT 'ราคาผู้ใหญ่',
  `room_price_child` double(5,2) NOT NULL COMMENT 'ราคาเด็ก',
  `room_picture1` text NOT NULL COMMENT 'รูป',
  `room_picture2` text NOT NULL COMMENT 'รูปภาพห้อง2',
  `room_picture3` text NOT NULL COMMENT 'รูปภาพห้อง3',
  `room_details` varchar(500) NOT NULL COMMENT 'รายละเอียดห้อง',
  `room_datesave` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'วันเวลาที่อัพเดทข้อมูลห้อง'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_room`
--

INSERT INTO `tb_room` (`room_id`, `room_name`, `room_price_adult`, `room_price_child`, `room_picture1`, `room_picture2`, `room_picture3`, `room_details`, `room_datesave`) VALUES
(1, 'ห้องเล็กริมคลอง', 450.00, 200.00, 'p1.jpg', 'image/img_room/p1.jpg', 'image/img_room/p1.jpg', 'ความสุขที่เรียบง่าย เที่ยวพักบ้านสวน  เหมือนมาเที่ยวบ้านพี่น้องกันเอง\r\nนอนมุ้งขนำริมคลอง ล่องแพกอนโดล่า ตักบาตรเช้าริมคลอง\r\nฟรี ผลไม้ในสวน สอยเก็บทานเองเลยจ้า\r\nฟรี เตาปิ้งย่างพร้อมถ่าน\r\nฟรี แก้วจานช้อนน้ำดื่มน้ำแข็งตักเองได้ทั้งคืน\r\nฟรี เสื้อชูชีพเล่นน้ำ\r\nฟรี เรือคายัคพายเล่น\r\nฟรี ที่นอนลอยน้ำ\r\nฟรี จักรยานปั่นรอบๆหมู่บ้าน', '2021-09-01 03:43:54'),
(2, 'ห้องกลางครอบครัว', 500.00, 250.00, 'p2.jpg', 'image/img_room/p2.jpg', 'image/img_room/p2.jpg', 'ใครกำลังมองหาที่พักชิล ๆ เน้นฟีลธรรมชาติ เราขอแนะนำที่นี่เลย ที่พักตั้งอยู่ติดริมลำธาร \r\nได้นอนฟังเสียงน้ำไหลมีกิจกรรมให้ได้ทำสนุก ๆ เพียบ!!! \r\nอย่างการที่เราออกไปล่องห่วงยางชิล ๆ ถ้าได้จิบเครื่องดื่มเย็น ๆ ไปด้วยนะ บอกเลยว่าฟินนนน\r\nถึงแม้บรรยากาศโดยรวมของทีพักเหมือนอยู่ในป่า ท่ามกลางธรรมชาติ \r\nแต่ภายในที่พักนั้นบอกเลยว่าสะดวกสบายมาก ๆ เตียงกว้าง นอนสบาย \r\nไม่ต้องกลัวเลยว่าจะลำบาก เพราะสิ่งอำนวยความสะดวกครบครัน', '2021-09-01 03:43:54'),
(3, 'ห้องใหญ่ดูดาว', 600.00, 300.00, '53531954420211005_133131.jpg', 'image/img_room/p3.jpg', 'image/img_room/p3.jpg', 'ใครชอบฟิล - สวน - ร่มรื่น - ฟังเสียงน้ำตก - จิบน้ำชาชิลๆ ต้องมาให้ได้ \r\nแม่ไพโฮมสเตย์ มีหลายโซนให้ถ่ายรูปพักผ่อนแบบชิลๆ ที่นี่มีสัญญาณมือถือ \r\nแต่.........อ่อนนะ แนะนำเคลียงาน เคลียธุระก่อนมาที่นี่ บางจุดไม่มีสัญญาณจริงๆ \r\nดับสนิท ยิ่งเข้าไปลึก ยิ่งไม่มีสัญญาณ ', '2021-09-01 03:43:54');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `user_id` int(10) NOT NULL COMMENT 'ไอดีผู้ใช้',
  `user_username` varchar(30) NOT NULL COMMENT 'ชื่อผู้ใช้',
  `user_password` varchar(50) NOT NULL COMMENT 'รหัสผ่าน',
  `user_name` varchar(60) NOT NULL COMMENT 'ชื่อ',
  `user_surname` varchar(60) NOT NULL COMMENT 'นามสกุล',
  `user_sex` enum('ชาย','หญิง') NOT NULL COMMENT 'เพศ',
  `user_birthdate` date NOT NULL COMMENT 'วันเกิด',
  `user_phone` varchar(10) NOT NULL COMMENT 'เบอร์โทรศัพท์',
  `user_line` varchar(30) NOT NULL COMMENT 'ไลน์',
  `user_facebook` varchar(100) NOT NULL COMMENT 'เฟสบุ๊ค',
  `user_email` varchar(100) DEFAULT NULL COMMENT 'อีเมล์',
  `user_level` enum('member','admin') NOT NULL DEFAULT 'member' COMMENT 'ระดับผู้ใช้'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`user_id`, `user_username`, `user_password`, `user_name`, `user_surname`, `user_sex`, `user_birthdate`, `user_phone`, `user_line`, `user_facebook`, `user_email`, `user_level`) VALUES
(1, 'admin', 'admin', 'ภีรภัทร', 'ศรีมั่น', 'ชาย', '1999-07-12', '0993189273', 'rusdee_psn', 'peerapat', 'peerapat@gmail.com', 'admin'),
(2, 'member', 'member', 'แพทริค', 'ฟิงค์เลอร์', 'ชาย', '2003-10-20', '0991234567', 'ppatrick', 'ppatrick', 'patrick@gmail.com', 'member'),
(17, 'keawwisad', '123', 'นาย แก้ววิเศษพงศ', 'สงวนวงศ์', 'ชาย', '0000-00-00', '+668964603', 'keaw', 'keaw', 'keaw3369@gmail.com', 'member'),
(18, 'keaw', '21232f297a57a5a743894a0e4a801fc3', 'นาย แก้ววิเศษพงศ', 'สงวนวงศ์', 'หญิง', '2021-10-28', '+668964603', 'keaw', 'keaw', 'keaw3369@gmail.com', 'member'),
(19, 'weqeqweqweqweqwe', '488bb1940ad1ee82ba077a3e0aefb19c', 'นาย แก้ววิเศษพงศdasdwada', 'dsadawdas', 'ชาย', '2021-10-20', '+668964603', 'keaw', 'keaw', 'keaw3369@gmail.com', 'member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookingactivitydetail`
--
ALTER TABLE `bookingactivitydetail`
  ADD PRIMARY KEY (`activity_dt_id`);

--
-- Indexes for table `bookingroomdetail`
--
ALTER TABLE `bookingroomdetail`
  ADD PRIMARY KEY (`room_dt_id`);

--
-- Indexes for table `tb_activity`
--
ALTER TABLE `tb_activity`
  ADD PRIMARY KEY (`activity_id`);

--
-- Indexes for table `tb_booking`
--
ALTER TABLE `tb_booking`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `tb_room`
--
ALTER TABLE `tb_room`
  ADD PRIMARY KEY (`room_id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookingactivitydetail`
--
ALTER TABLE `bookingactivitydetail`
  MODIFY `activity_dt_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `bookingroomdetail`
--
ALTER TABLE `bookingroomdetail`
  MODIFY `room_dt_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `tb_activity`
--
ALTER TABLE `tb_activity`
  MODIFY `activity_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสกิจกรรม', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_booking`
--
ALTER TABLE `tb_booking`
  MODIFY `book_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- AUTO_INCREMENT for table `tb_room`
--
ALTER TABLE `tb_room`
  MODIFY `room_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'รหัสห้อง', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'ไอดีผู้ใช้', AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
