<?php
$q = $_GET['q'];

$sql = "SELECT * FROM tb_room WHERE room_id <= 3 ORDER BY room_id DESC";
$result = mysqli_query($con, $sql);

$sql1 = "SELECT * FROM tb_activity WHERE activity_id <= 3 ORDER BY activity_id DESC";
$result1 = mysqli_query($con, $sql1);
// echo $sql . '<br>';
// print_r($result);
?>
<div class="container">
    <div class="row" style="padding-top:33px; padding-bottom:33px">
        <div class="col-md-6 col-sm-12">
            <div class="about_info">
                <div class="section_title mb-20px">
                    <h2>แม่ไพโฮมสเตย์ ล่องแพกอนโดล่า</h2>
                    <p>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ตื่นเช้ามาพร้อมกับหมอกบนน้ำ ล้อมรอบด้วยธรรมชาติริมคลอง ไม่ว่าจะ กิน พัก เที่ยว ล่องเเพ 
                        เเม่ไพโฮมสเตย์พร้อมมอบความสุขให้คุณเเละครอบครัว ทานอาหารบนเเพชมธรรมชาติในตอนเช้าหรือจะล่องเเพชมหิ่งห้อยในตอนกลางคืน พร้อมเสิร์ฟความน่ารักของคนล่องเเพที่เป็นกันเอง อยากให้คุณได้ลองสัมผัสบรรยากาศบ้านๆ สุดเเสนพิเศษด้วยเเม่ไพ โฮมสเตย์
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 d-flex">
            <div class="col-sm-6 img_1">
                <img src="image/img1.png" alt="" width="230px">
            </div>
            <div class="col-sm-6 img_2">
                <img src="image/img2.png" alt="" width="230px">
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding:20px 200px 20px 200px; background-color: #D3D3D3;">
    <div class="row">
        <h2>โฮมสเตย์</h2>

    </div>
    <div class="row">
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;แม่ไพโฮมสเตย์ กิจกรรมดีๆที่พักโดนๆ ให้ทุกคน ได้มาสนุกกับการล่องแพกอนโดล่า และที่พักสุดธรรมชาติที่มีห้องให้คุณเลือกหลายเเบบไม่ว่าจะเป็นห้องดูดาว ห้องครอบครัว เเม้เเต่ห้องริมคลอง จะมากับคนรักหรือครอบครัวการันตรีเลยว่า เเม่ไพ โฮมสเตย์ตอบโจทย์คุณได้แน่นอน</p>
    </div>

    <?php
    while ($row_prd_room = mysqli_fetch_array($result)) {
    ?>
        <div class="col-lg-4">
            <div class="section border bg-white rounded p-2">
                <div class="row">
                    <div class="col-lg-12 img-section">
                        <img src="image/img_room/<?php echo $row_prd_room["room_picture1"]; ?>" class="p-0 m-0 res-ponsive" style="height: 200px;">
                        <span class="badge badge-danger add-sens p-2 rounded-0">NOW</span>
                    </div>
                    <div class="col-lg-12 sectin-title">
                        <h1 class="pt-2 pb-2"><?php echo $row_prd_room["room_name"]; ?></h1>
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-2">
                                <span class="badge badge-success p-2"><?php echo "ราคาเริ่มที่ " . $row_prd_room["room_price_adult"] . " บาท"; ?></span>
                            </div>
                            <div class="col-lg-10 text-right">
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star-half-alt"></i></span><br>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="col-lg-12 pb-2">
                        <div class="row">

                            <div class="col-lg-12">
                                <a href="product_room.php" class="btn btn-info btn-block btn-sm">เลือกดูโฮมสเตย์</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>
<div class="row" style="padding:20px 200px 20px 200px; background-color: #D3D3D3;">
    <div class="row">
        <h2>กิจกรรม</h2>

    </div>
    <div class="row">
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;แม่ไพโฮมสเตย์พร้อมให้บริการทั้งด้านห้องพักและกิจกรรมต่างๆมากมายที่ให้ทุกคนสามารถร่วมสนุกและกิจกรรมของเราได้โดยสามารถเลือกจองล่วงโฮมสเตย์ ที่ครบ จบบนเเพในที่เดียว ต้อง เเม่ไพ โฮมสเตย์ ให้บริการด้านที่พักฉบับบ้านสวน พร้อมกิจกรรมนั่งเเพชมธรรมชาติริมคลองราชา และชุดอาหารสุดพิเศษ ที่สามารถทานบนเเพได้เลย สำหรับท่านใดที่สนใจสามารถจองล่วงหน้าเพื่อสำรองที่ไว้ ทางโฮมเตย์จะดูเเลเเละใส่ใจคุณเหมือนคนในครอบครัวอย่างเต็มที่ อย่าลืมมาเที่ยวกันนะครับ </p>
        
    </div>

    <?php
    while ($row_prd_activity = mysqli_fetch_array($result1)) {
    ?>
        <div class="col-lg-4">
            <div class="section border bg-white rounded p-2">
                <div class="row">
                    <div class="col-lg-12 img-section">
                        <img src="image/img_activity/<?php echo $row_prd_activity["activity_picture1"]; ?>" class="p-0 m-0 res-ponsive" style="height: 200px;">
                        <span class="badge badge-danger add-sens p-2 rounded-0">NOW</span>
                    </div>
                    <div class="col-lg-12 sectin-title">
                        <h1 class="pt-2 pb-2"><?php echo $row_prd_activity["activity_name"]; ?></h1>
                    </div>
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-2">
                                <span class="badge badge-success p-2"><?php echo "ราคาเริ่มที่ " . $row_prd_activity["activity_price_adult"] . " บาท"; ?></span>
                            </div>
                            <div class="col-lg-10 text-right">
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star-half-alt"></i></span><br>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="col-lg-12 pb-2">
                        <div class="row">

                            <div class="col-lg-12">
                                <a href="product_activity.php" class="btn btn-info btn-block btn-sm">เลือกดูกิจกรรม</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
    }
    ?>
</div>